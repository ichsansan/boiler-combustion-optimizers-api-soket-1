import db_config
import random
import numpy as np
import pandas as pd
import sys
import os
import config
import sqlalchemy as db
from file_manager_helper import load_forecasting_model, load_generative_model_xgb, load_tag_contract, load_model_converter
from contracts.reducers.dataset_contract_reducers import convert_dataset, scale_back
from contracts.tag_contract_factory import TagContract
from sklearn.pipeline import Pipeline
from typing import Dict, List, Tuple, Any
import pywt
from datetime import timedelta
import traceback
import time
import joblib

from pyXSteam.XSteam import XSteam
steamTable = XSteam(XSteam.UNIT_SYSTEM_MKS)


class MainFlow:
    def __init__(self, unit, debug_mode, fetch_realtime=False):
        self.db_mapping = db_config.DB_MAPPING[unit]

        self.unit = unit
        self.current_forecasting_model_names = config.FORECASTING_MODEL_CURRENT_NAMES[unit]
        self.current_generative_model_name = config.GENERATIVE_MODEL_CURRENT_NAME[unit]

        self.current_dataset_tag_contract = load_tag_contract(
            contract_alias=self.unit,
            directory=os.path.join(config.ZIP_DATASET_CONTRACTS_DIRECTORY, self.unit))

        self.engine = db.create_engine(
            f"mysql+pymysql://{self.db_mapping['username']}:{self.db_mapping['password']}@{self.db_mapping['host']}/{self.db_mapping['db_name']}",
            echo=False)
        self.debug_mode = debug_mode
        self.fetch_realtime = fetch_realtime
        self.mv_recomendations_df = None
        self.time_logs = {}

        # Oxygen callibration
        self.O2_A_intercept_ = 1.68442643796
        self.O2_A_coef_ = 0.1679237
        self.O2_B_intercept_ = 1.495081938
        self.O2_B_coef_ = -0.422954354

    def load_saved_data(self):
        print('################# LOADING SAVED DATA INSIDE LOCAL API #################', flush=True)
        start_time = time.time()

        self.engine_data_target = db.create_engine(
            f"mysql+pymysql://{self.db_mapping['username']}:{self.db_mapping['password']}@{self.db_mapping['host']}/{self.db_mapping['db_name']}",
            echo=False)
        latest_id = get_latest_id(self.engine_data_target, "tb_combustion_model_logs", "process_id")

        self.engine_data_target.dispose()

        if latest_id is not None:
            self.process_id = latest_id + 1
        else:
            self.process_id = 0

        self.logger_dict = {
            'process_id': [],
            'name': [],
            'input': [],
            'output': [],
        }

        # load MOT configuration
        mit_config, mot_config = get_mit_config(
            self.engine_data_target, f"{self.db_mapping['db_name']}.tb_combustion_conf_tags")
        self.mill_inlet_temp_config = mit_config
        self.mill_outlet_temp_config = mot_config
        self.current_mill_outlet_temperature = mot_config[['description', 'value']].set_index('description')['value']

        # Loading target and directional variable models
        forecasting_models = dict()

        for forecasting_model_name_key in self.current_dataset_tag_contract.tag_groups_map[
                "RECOMMENDATION_TARGET_VARIABLES"]:
            forecasting_model_name = self.current_forecasting_model_names[forecasting_model_name_key]
            forecasting_model_dir = os.path.join(
                config.ZIP_FORECASTING_MODELS_DIRECTORY,
                self.unit,
                forecasting_model_name_key,
                forecasting_model_name)

            forecasting_models[forecasting_model_name] = load_forecasting_model(
                forecasting_model_dir, forecasting_model_name)

        for forecasting_model_name_key in self.current_dataset_tag_contract.tag_groups_map[
                "RECOMMENDATION_DIRECTION_VARIABLES"]:
            forecasting_model_name = self.current_forecasting_model_names[forecasting_model_name_key]
            forecasting_model_dir = os.path.join(
                config.ZIP_FORECASTING_MODELS_DIRECTORY,
                self.unit,
                forecasting_model_name_key,
                forecasting_model_name)

            forecasting_models[forecasting_model_name] = load_forecasting_model(
                forecasting_model_dir, forecasting_model_name)

        self.forecasting_models = forecasting_models

        generative_model_dir = os.path.join(config.ZIP_GENERATIVE_MODELS_DIRECTORY, self.unit)
        self.generative_model = load_generative_model_xgb(self.current_generative_model_name, generative_model_dir)

        # sensor list
        sensor_df = pd.read_csv('data/sensor_list.csv', index_col='Unit')
        self.sensor_df = sensor_df.loc[self.unit]
        self.sensors = self.sensor_df.loc[:, 'Tag'].values.tolist()

        # this variable only used to fetch data from server database due to unit differentiation in sensor reading
        # self.alias_sensors = self.sensor_df.loc[:, 'Alias Tag From DB'].values.tolist()

        end_time = time.time()
        self.time_logs['Load Saved Data'] = f'{(end_time - start_time)} secs'

    def read_realtime_data(self, n_minutes):
        """This function is used to fetch the realtime and short historical data from the database."""
        print('################# READ REALTIME DATA FROM DATABASE #################', flush=True)
        start_time = time.time()

        model_converters_dir = os.path.join(config.ZIP_MODEL_CONVERTERS_DIRECTORY, self.unit)
        self.model_converter_input_tags = dict()
        self.model_converters = dict()

        for model_converter_tag in config.MODEL_CONVERTER_NAMES[self.unit]:
            model_converter, model_converter_input_tags = load_model_converter(
                model_converter_tag, model_converters_dir)
            self.model_converters[model_converter_tag] = model_converter
            self.model_converter_input_tags[model_converter_tag] = model_converter_input_tags

        if self.fetch_realtime:
            realtime_df, original_realtime_df = get_realtime_data(
                unit=self.unit,
                engine=self.engine,
                table_name=self.db_mapping['tb_data_source'],
                sensor_tags=self.sensors,
                timesteps=n_minutes,
                contract=self.current_dataset_tag_contract,
                model_converters=self.model_converters,
                model_converter_input_tags=self.model_converter_input_tags)
        else:
            realtime_df = pd.read_csv('data//debugging_outputs//Realtime DF.csv',
                                      index_col='timestamp', parse_dates=['timestamp'])
            original_realtime_df = pd.read_csv(
                'data//debugging_outputs//Original Realtime DF.csv', index_col='timestamp', parse_dates=['timestamp'])

        self.realtime_df = realtime_df
        self.original_realtime_df = original_realtime_df

        end_time = time.time()
        self.time_logs['Fetch Sensor Data'] = f'{(end_time - start_time)} secs'

    def read_from_disk_realtime_data(self, table_name: str, timesteps: int, random_num: int):
        """This function is being used just for deriving the data from the local disk instead from the database.
           Thus it should have the same output as 'read_realtime_data' and we do NOT deploy this function, because
           it is onlt for local use."""
        print('################# READ REALTIME DATA FROM LOCAL DISK #################', flush=True)
        start_time = time.time()

        model_converters_dir = os.path.join(config.ZIP_MODEL_CONVERTERS_DIRECTORY, self.unit)
        self.model_converter_input_tags = dict()
        self.model_converters = dict()

        for model_converter_tag in config.MODEL_CONVERTER_NAMES[self.unit]:
            model_converter, model_converter_input_tags = load_model_converter(
                model_converter_tag, model_converters_dir)
            self.model_converters[model_converter_tag] = model_converter
            self.model_converter_input_tags[model_converter_tag] = model_converter_input_tags

        realtime_df, original_realtime_df = get_from_disk_realtime_data(
            random_num=random_num,
            unit=self.unit,
            table_name=table_name,
            timesteps=timesteps,
            contract=self.current_dataset_tag_contract,
            model_converters=self.model_converters,
            model_converter_input_tags=self.model_converter_input_tags)

        self.realtime_df = realtime_df
        self.original_realtime_df = original_realtime_df

        end_time = time.time()
        self.time_logs['Fetch Sensor Data'] = f'{(end_time - start_time)} secs'

    def pre_calculate_tags(self):
        # print('################# PRE-CALCULATION #################', flush=True)

        # TEMPORARY! - Edit parameter Efficiency
        self.realtime_df['Hot Reheat Steam Flow'] = abs(self.realtime_df['Hot Reheat Steam Flow'])

        # Change Efficiency Variables
        # if 'Efficiency' in self.original_realtime_df.columns:
        #     self.realtime_df['Efficiency'] = self.original_realtime_df['Efficiency']

        # Fix burner tilt values
        unit_no = self.unit[-1]
        self.realtime_df['Burner Tilt Position 0'] = self.original_realtime_df[f'ZT{unit_no}C030101_PNT']
        self.realtime_df['Burner Tilt Position 1'] = self.original_realtime_df[f'ZT{unit_no}C030401_PNT']
        self.realtime_df['Burner Tilt Position 2'] = self.original_realtime_df[f'ZT{unit_no}C030801_PNT']
        self.realtime_df['Burner Tilt Position 3'] = self.original_realtime_df[f'ZT{unit_no}C031401_PNT']

        # Change O2 value to average both sensors
        # valid_o2 = self.realtime_df[[config.EXCESS_OXYGEN_1_TAG, config.EXCESS_OXYGEN_2_TAG]].mean(axis=1)
        # O2 conversion based on Portable Analyzer
        self.realtime_df[config.EXCESS_OXYGEN_1_TAG] = self.realtime_df[config.EXCESS_OXYGEN_1_TAG] * self.O2_A_intercept_ + self.O2_A_coef_
        self.realtime_df[config.EXCESS_OXYGEN_2_TAG] = self.realtime_df[config.EXCESS_OXYGEN_2_TAG] * self.O2_A_intercept_ + self.O2_A_coef_

        realtime_calc_df = self.realtime_df.copy()

        # Giving Coal HHV estimate values
        hhv_est = (
            self.realtime_df['Generator Gross Megawatt'] * 6.7721145661919 + (self.realtime_df['Coal Flow'] * 3.6) * -
            10.903148890184394) + 4284.626263834663  # Scale back Coal Flow
        realtime_calc_df['Coal HHV Estimate'] = hhv_est
        self.realtime_df['Coal HHV'] = hhv_est

        # Giving Total Moisture estimate
        tm_est = -0.0116272626573054 * hhv_est + 83.1161457080681
        realtime_calc_df['Total Moisture'] = tm_est
        current_coal_moisture = realtime_calc_df['Total Moisture'].iloc[-1]

        self.realtime_df = self.realtime_df.ffill().bfill()

        # Calculate Active Mill Outlet Temp.
        if config.IS_MOT_ESTIMATOR[self.unit]:
            mot_est = joblib.load(os.path.join(config.MOT_ESTIMATOR_DIRECTORY,
                                               self.unit,
                                               f"{config.MOT_MODEL_NAME[self.unit]}_pipeline.bin"))

            current_mill_inlet_temp = {}
            current_mill_outlet_temp = {}
            for c in np.unique(self.mill_inlet_temp_config['description']):
                mit_per_c_df = self.mill_inlet_temp_config[self.mill_inlet_temp_config['description'] == c]
                current_mill_inlet_temp[c] = np.average([float(f) for f in mit_per_c_df['value'] if float(f) > 150])

                try:
                    current_mill_outlet_temp[c.replace('Inlet', 'Outlet')] = float(
                        mot_est.predict([[current_coal_moisture, current_mill_inlet_temp[c]]]))
                except ValueError:
                    current_mill_inlet_temp[c.replace('Inlet', 'Outlet')] = 0.0

            self.mill_outlet_temperature_suggestion = current_mill_outlet_temp
        else:
            self.mill_outlet_temperature_suggestion = {}

        if self.debug_mode:
            print(f'Gross Load     : {self.realtime_df["Generator Gross Megawatt"].values[-1]} MW')
            print(f'Coal Flow      : {self.realtime_df["Coal Flow"].values[-1]*3.6} T/h')
            print(f'HHV Estimation : {realtime_calc_df["Coal HHV Estimate"].values[-1]}')
            print(f'Total Moisture : {realtime_calc_df["Total Moisture"].values[-1]} %')
            print('Mill Outlet suggestion:')
            print(pd.Series(self.mill_outlet_temperature_suggestion))
            print()
        return

    def post_calculate_tags(self):
        print("######## POST CALCULATE TAGS ########")
        if self.mv_recomendations_df is not None:
            # for c in [config.EXCESS_OXYGEN_1_TAG, config.EXCESS_OXYGEN_2_TAG]:
            #     if c not in self.predicted_excess_oxy.keys():
            #         continue
            #     self.predicted_excess_oxy[c] = (self.predicted_excess_oxy[c] - self.O2_coef_) / self.O2_intercept_
            #     self.realtime_df[c] = (self.realtime_df[c] - self.O2_coef_) / self.O2_intercept_
            return
            self.predicted_excess_oxy[config.EXCESS_OXYGEN_1_TAG] = (
                self.predicted_excess_oxy[config.EXCESS_OXYGEN_1_TAG] - self.O2_A_coef_) / self.O2_A_intercept_
            self.predicted_excess_oxy[config.EXCESS_OXYGEN_2_TAG] = (
                self.predicted_excess_oxy[config.EXCESS_OXYGEN_2_TAG] - self.O2_B_coef_) / self.O2_B_intercept_
            self.predicted_excess_oxy_df = pd.DataFrame(pd.Series(self.predicted_excess_oxy)).T
        return

    def predict_inverse_mapping(self):
        print('################# PREDICT #################', flush=True)
        start_time = time.time()

        prediction = Prediction(
            self.unit,
            self.current_forecasting_model_names,
            self.forecasting_models,
            self.generative_model,
            self.current_dataset_tag_contract)
        message = ""
        mv_recomendations_df = pd.DataFrame()
        models_predictions = None
        self.predicted_excess_oxy = None
        self.predicted_target = None
        curr_efficiency = None

        try:
            prediction.data_preparation(self.realtime_df)
            data_flats = Prediction.initial_flat_data_checking(prediction.realtime_df)
            (cvs_within_limits, not_passed_tags) = prediction.cvs_within_limit_checking(prediction.target_variables)

            if len(data_flats) > 0:
                message += f"\The following tag pairs is flat: '{str(data_flats)}'. Please check if the sensor(s) is offline / deactivated."
            elif cvs_within_limits:
                # TODO: Include the range as well.
                message += f" \nThe CV current values are within the desired range ({self.realtime_df[prediction.target_variables].iloc[-1]})."
            else:
                message += f"There are CV(s) ({not_passed_tags}) outside the optimal range and thus we will generate MV recommendations."
                (not_all_conditions_fullfilled, constrans_not_passing) = prediction.initial_constraint_checking()

                # Force constrains
                # not_all_conditions_fullfilled = False
                # constrans_not_passing = {}

                if not_all_conditions_fullfilled:
                    message += f"\The following tag-value pairs don't fulfill the initial constraints: '{str(constrans_not_passing)}'."
                else:
                    if not_all_conditions_fullfilled:
                        message += f"\The following tag-value pairs don't fulfill the initial constraints: '{str(constrans_not_passing)}'."
                    else:
                        prediction.mv_generation()

                    mv_recomendations_df, models_predictions, self.optimizers = prediction.cv_forecasting()

                    if mv_recomendations_df is None or mv_recomendations_df.shape[0] == 0:
                        if config.EFFICIENCY_TAG in list(prediction.target_variables):
                            message += " \nEfficiency increase is estimated to be not possible."

                        if any(
                            target in prediction.target_variables
                            for target
                            in
                            [config.EXCESS_OXYGEN_1_TAG, config.EXCESS_OXYGEN_2_TAG, config.EXCESS_OXYGEN_1_TAG_B,
                             config.EXCESS_OXYGEN_2_TAG_B]):
                            message += " \nJump towards excess oxygen optimality is estimated to not be possible."
                        if not prediction.any_mv_constraint_check_satisfied:
                            message += " \nSome of the MV constraints are not being met."
                        if not prediction.any_model_constraint_check_satisfied:
                            message += " \nThe data doesn't fit the models capacity."
                        if not prediction.any_dv_constraints_check_satisfied:
                            message += "\nExcess Oxygen jump is larger than set limits."
                    else:
                        # TODO: Provide list of the excess oxygens in a contract.

                        if any(
                            target in prediction.target_variables
                            for target
                            in
                            [config.EXCESS_OXYGEN_1_TAG, config.EXCESS_OXYGEN_2_TAG, config.EXCESS_OXYGEN_1_TAG_B,
                             config.EXCESS_OXYGEN_2_TAG_B]):

                            excess_oxy_1_model_name = self.current_forecasting_model_names[list(prediction.target_variables)[
                                0]]
                            excess_oxy_2_model_name = self.current_forecasting_model_names[list(prediction.target_variables)[
                                1]]
                            efficiency_model_name = self.current_forecasting_model_names[config.EFFICIENCY_TAG]
                            if excess_oxy_1_model_name in models_predictions or \
                                    excess_oxy_2_model_name in models_predictions:

                                excess_oxy_dist = 0
                                excess_oxy_current_state_dist = 0
                                excess_oxy_dist_count = 0
                                for excess_oxy_tag in prediction.target_variables:
                                    model_name = self.current_forecasting_model_names[excess_oxy_tag]

                                    if model_name in self.optimizers:
                                        optimizer = self.optimizers[model_name]
                                        excess_oxy_dist += optimizer.min_excess_oxy_dist
                                        excess_oxy_current_state_dist += optimizer.current_state_excess_oxy_dist
                                        excess_oxy_dist_count += 1

                                excess_oxy_dist /= excess_oxy_dist_count
                                excess_oxy_current_state_dist /= excess_oxy_dist_count
                                message += f" \nWe found a solution that could change the excess oxygen distance from the sweet spot from " + \
                                    f"{excess_oxy_current_state_dist} to {excess_oxy_dist}." + \
                                    f" \nThe estimated change is for the next {config.LABEL_LAG} minutes."

                                # Assuming sweet spot is the same for any of the optimizers
                                self.predicted_excess_oxy = optimizer.sweet_spot + excess_oxy_dist

                        if config.EFFICIENCY_TAG in prediction.target_variables:
                            # Assigning excess oxygen tags to their corresponding predictions after MV change.
                            self.predicted_excess_oxy = {dv: models_predictions[prediction.forecasting_model_names[dv]]
                                                         for dv in prediction.direction_variables}
                            self.predicted_target = {cv: models_predictions[prediction.forecasting_model_names[cv]]
                                                     for cv in prediction.target_variables}

                            efficiency_model_name = self.current_forecasting_model_names[config.EFFICIENCY_TAG]
                            max_horizon_eff_diff = self.optimizers[efficiency_model_name].max_horizon_eff_diff
                            if max_horizon_eff_diff > 0:
                                # It might be that we can increase efficiency, but Excess Oxygen
                                # variables do not go towards favourable direction
                                if not prediction.any_dv_constraints_check_satisfied:
                                    message += "\nExcess Oxygen not forecasted to go in correct direction with one or both variables with following recommendation."
                                message += f" \nThe solution optimizes based on efficiency which could increase by {self.optimizers[efficiency_model_name].max_horizon_eff_diff} ." + \
                                    f" \nThe estimated change is for the next {config.LABEL_LAG} minutes."
                            else:
                                # the efficiency diff is negative
                                message += f" \nThe solution optimizes based on efficiency which best found solutions is efficiency decrease by {self.optimizers[efficiency_model_name].max_horizon_eff_diff}."
        except Exception as e:
            message = traceback.format_exc()

        curr_efficiency = self.realtime_df.tail(1)[config.EFFICIENCY_TAG].values[0]
        curr_efficiency = np.minimum(curr_efficiency, config.EFFICIENCY_CAP)
        curr_efficiency = f'{curr_efficiency} %'

        if mv_recomendations_df is not None:
            self.mv_recomendations_df = scale_back(
                contract=self.current_dataset_tag_contract,
                dataset=mv_recomendations_df)

        end_time = time.time()
        self.time_logs['Prediction Time'] = f'{(end_time - start_time)} secs'

        return message, curr_efficiency

    def save_to_db(self):
        print('################# SAVE TO DATABASE #################', flush=True)
        start_time = time.time()

        if self.debug_mode:
            self.engine.dispose()
            self.engine = db.create_engine(
                f"mysql+pymysql://root:@127.0.0.1/{self.db_mapping['db_name']}", echo=False)

        mv_generation_dict = {
            'model_id': [],
            'ts': [],
            'tag_name': [],
            'value': [],
            'bias_value': [],
        }

        predicted_excess_oxy_dict = {
            'model_id': [],
            'ts': [],
            'tag_name': [],
            'value': [],
        }

        predicted_efficiency_dict = {
            'model_id': [],
            'ts': [],
            'efficiency_value': [],
            'horizon_step': [],
        }

        session_dict = {
            'ts': [],
            'tag_name': [],
            'category': [],
            'value': [],
            'history_step': [],
        }

        recommendation_mv_variables = self.current_dataset_tag_contract.tag_groups_map["RECOMMENDATION_MV_VARIABLES"]
        mill_outlet_constranins = self.current_dataset_tag_contract.tag_groups_map["MILL_OUTLET_CONSTRAINTS"]

        if self.fetch_realtime:
            self.latest_timestamp = self.realtime_indexes[-1]
        else:
            # FOR TESTING PURPOSE!
            self.latest_timestamp = pd.to_datetime('today').floor('T')

        if self.mv_recomendations_df is not None:
            self.sensor_df.set_index('Tag', inplace=True)

            sensor_input_df = self.realtime_df[recommendation_mv_variables]
            sensor_input_df = sensor_input_df.iloc[-1:]
            self.delta_mv_recomendations_df = Prediction.calculate_delta_mv(sensor_input_df, self.mv_recomendations_df)

            for col in self.mv_recomendations_df.columns:
                mv_generation_dict['ts'].append(self.latest_timestamp)
                mv_generation_dict['model_id'].append(
                    config.GENERATIVE_MODEL_CURRENT_NAME[self.unit])
                mv_generation_dict['tag_name'].append(col)
                mv_generation_dict['value'].append(self.mv_recomendations_df.loc[0, col])
                mv_generation_dict['bias_value'].append(self.delta_mv_recomendations_df.loc[0, col])

            mv_generation_df = pd.DataFrame(mv_generation_dict)
            mv_generation_df['tag_name'] = mv_generation_df['tag_name'].astype(
                str)
            mv_generation_df.to_csv('tb_combustion_model_generation.csv')
            # mv_generation_df.to_sql(
            #     "tb_combustion_model_generation", con=self.engine, method=mysql_replace_into, \
            #     if_exists="append", index=False)

            if self.predicted_excess_oxy is not None:
                for idx, val in self.predicted_excess_oxy.items():
                    predicted_excess_oxy_dict['ts'].append(self.latest_timestamp)
                    predicted_excess_oxy_dict['model_id'].append(
                        config.GENERATIVE_MODEL_CURRENT_NAME[self.unit])
                    predicted_excess_oxy_dict['tag_name'].append(idx)
                    predicted_excess_oxy_dict['value'].append(val)

                predicted_excess_oxy_df = pd.DataFrame(predicted_excess_oxy_dict)
                predicted_excess_oxy_df['tag_name'] = predicted_excess_oxy_df['tag_name'].astype(
                    str)
                # predicted_excess_oxy_df.to_sql(
                #     "tb_combustion_model_generation", con=self.engine, method=mysql_replace_into, \
                #     if_exists="append", index=False)

            elif self.predicted_efficiency is not None:
                for i in range(self.predicted_efficiency.shape[2]):
                    predicted_efficiency_dict['ts'].append(
                        self.latest_timestamp + timedelta(minutes=i+1))
                    predicted_efficiency_dict['model_id'].append(
                        config.EFFICIENCY_FORECASTING_MODEL_CURRENT_NAME[self.unit])
                    predicted_efficiency_dict['efficiency_value'].append(
                        self.predicted_efficiency[0, 0, i])
                    predicted_efficiency_dict['horizon_step'].append(i+1)

                predicted_efficiency_df = pd.DataFrame(predicted_efficiency_dict)
                predicted_efficiency_df.to_csv('tb_combustion_model_forecasting.csv')
                # predicted_efficiency_df.to_sql("tb_combustion_model_forecasting",
                #     con=self.engine, method=mysql_replace_into,
                #     if_exists="append", index=False)

        counter_idx = 0

        recommendation_mv_variables = self.current_dataset_tag_contract.tag_groups_map["RECOMMENDATION_MV_VARIABLES"]
        mill_outlet_constranins = self.current_dataset_tag_contract.tag_groups_map["MILL_OUTLET_CONSTRAINTS"]

        for idx, val in self.realtime_df.iterrows():
            for col in self.realtime_df.columns:
                if self.fetch_realtime:
                    session_dict['ts'].append(idx)
                else:
                    # TEMPORARY USED!
                    session_dict['ts'].append(
                        self.latest_timestamp - timedelta(minutes=self.realtime_df.shape[0]-(counter_idx+1)))

                if col in recommendation_mv_variables:
                    category = 'MV'
                elif col in mill_outlet_constranins:
                    category = 'CONSTRAINT'
                else:
                    category = 'DV'

                session_dict['tag_name'].append(col)
                session_dict['category'].append(category)
                session_dict['value'].append(val[col])
                session_dict['history_step'].append(counter_idx+1)

            counter_idx += 1

        session_df = pd.DataFrame(session_dict)
        session_df['tag_name'] = session_df['tag_name'].astype(str)
        session_df.replace([np.inf, -np.inf], 0, inplace=True)

        # batch saving
        batch_size = 500
        curr_counter = 0

        while curr_counter < session_df.shape[0]:
            # session_df.iloc[curr_counter:(curr_counter+batch_size)].to_sql("tb_combustion_model_history",
            #               con=self.engine, method=mysql_replace_into, if_exists="append", index=False)
            curr_counter = curr_counter + batch_size

        # close db connection
        # self.engine.dispose()

        end_time = time.time()
        self.time_logs['Saving to DB Time'] = f'{(end_time - start_time)} secs'

    def get_time_logs(self):
        return self.time_logs


class Prediction:
    def __init__(self,
                 unit: str,
                 forecasting_model_names: Dict[str, str],
                 forecasting_models: Dict[str, Pipeline],
                 generative_model: Pipeline,
                 contract: TagContract):

        self.unit = unit
        self.target_variables = contract.tag_groups_map["RECOMMENDATION_TARGET_VARIABLES"]
        self.direction_variables = contract.tag_groups_map["RECOMMENDATION_DIRECTION_VARIABLES"]
        self.excess_oxy_sweet_spot = contract.tags_to_actions["GET_EXCESS_OXYGEN_SWEET_SPOT"]
        self.target_limits = contract.tags_to_actions["TARGET_LIMITS"]
        self.forecasting_model_names = forecasting_model_names
        self.optimizers = dict()

        for cv_tag in self.target_variables:
            if cv_tag in forecasting_model_names:
                model_name = forecasting_model_names[cv_tag]
                forecast_model = forecasting_models[model_name]
                gen_input_indices = {dv_tag: generative_model["model"].estimator_params["tags_to_index"][dv_tag]
                                     for dv_tag in self.direction_variables}
                limits = self.target_limits[cv_tag]

                if config.EFFICIENCY_TAG == cv_tag:
                    self.optimizers[model_name] = EfficiencyOptimizer(
                        forcast_input_index=forecast_model["model"].estimator_params["tags_to_index"][cv_tag],
                        gen_input_indices=gen_input_indices,
                        limits=limits,
                        dv_sweet_spot=self.excess_oxy_sweet_spot)
                # TODO: encapsulate that a contract
                elif cv_tag in {config.EXCESS_OXYGEN_1_TAG, config.EXCESS_OXYGEN_2_TAG, config.EXCESS_OXYGEN_1_TAG_B, config.EXCESS_OXYGEN_2_TAG_B}:
                    self.optimizers[model_name] = OxygenOptimizer(
                        gen_input_index=gen_input_indices,
                        sweet_spot=self.excess_oxy_sweet_spot[cv_tag],
                        limits=limits)
            else:
                raise ValueError(
                    f"The tag '{cv_tag}' specified in the contract is not a part of the recommedation system required tags to be forecast.'")

        self.forecasting_models = forecasting_models
        self.generative_model = generative_model
        self.recommendation_mv_variables = contract.tag_groups_map["RECOMMENDATION_MV_VARIABLES"]
        self.mv_dependent_constraints = contract.tag_groups_map["MV_DEPENDENT_CONSTRAINTS"]
        self.any_mv_constraint_check_satisfied = False
        self.any_dv_constraints_check_satisfied = False
        self.any_model_constraint_check_satisfied = False
        self.forecast_excess_oxy_within_limits = dict()

        self.const_limits = contract.tags_to_actions["CONST_LIMITS"]
        self.variables = contract.get_active_tags()

    def data_preparation(self, realtime_df):
        print('################# DATA PREPARATION #################', flush=True)
        if len(set(realtime_df.columns) - set(self.variables)) > 0:
            # TODO: system which is catching the errors and it is logging them in a storage needed.
            raise ValueError(
                f"The input data has {len(realtime_df.columns)} variables, but the contract has {len(self.variables)} variables.")

        generative_estimator_params = self.generative_model["model"].estimator_params
        generative_tags_to_index = generative_estimator_params["tags_to_index"]

        if len(self.variables) < len(generative_tags_to_index):
            raise ValueError(
                f"The contract variables number is {len(self.variables)}, but the variables of the generative model are {len(generative_tags_to_index)}.\n The extra values are {set(generative_tags_to_index) - set(self.variables)}")

        if "diff_lag" not in generative_estimator_params:
            diff_lag = 1
        else:
            diff_lag = generative_estimator_params["diff_lag"]

        forecasting_cv_history_length = -sys.maxsize - 1
        forecasting_mv_history_length = forecasting_cv_history_length
        for model_key in self.forecasting_models.keys():
            current_cv_history_length = self.forecasting_models[model_key]["model"].estimator_params["cv_history_length"]
            current_mv_history_length = self.forecasting_models[model_key]["model"].estimator_params["mv_history_length"]
            if forecasting_cv_history_length < current_cv_history_length:
                forecasting_cv_history_length = current_cv_history_length
            if forecasting_mv_history_length < current_mv_history_length:
                forecasting_mv_history_length = current_mv_history_length

            variables = self.forecasting_models[model_key]["model"].estimator_params["tags_to_index"].keys()
            if len(set(variables) - set(self.variables)) > 0:
                raise ValueError(
                    f"The contract variables number is {len(self.variables)}, but the variables of the forecasting model '{model_key}' are {len(variables)}")

        # Extract the days and minutes
        first_day, first_minute = extract_day_min_from_date(
            realtime_df.index[0])
        days = np.empty((realtime_df.shape[0], ))
        minutes = np.empty((realtime_df.shape[0], ))

        current_day = first_day

        for i in range(realtime_df.shape[0]):
            current_min = (first_minute + i) % config.MINUTES_IN_A_DAY

            if current_min == 0:
                current_day += 1

            days[i] = current_day
            minutes[i] = current_min

        tags_to_index = dict()

        tags = []

        for i, sensor_tag in enumerate(realtime_df.columns):

            if self.unit == config.PAITON1_DATA_TAG:
                # Denoise with wavelets (the model has been traind with denoised signals)
                # We use denoising only for PAITON, because the Awar Awar Bay data is not suitable.
                # TODO: Investigate why the Awar Awar Bay data is not good for denoising.
                # TODO: hardcode in config the length of the sensor history in order to be sufficient for denoising
                realtime_df[sensor_tag] = noise_reduction(
                    realtime_df[sensor_tag].values, days)

            if sensor_tag in variables:
                tags_to_index[sensor_tag] = len(tags)
                tags.append(sensor_tag)

        # '+1', because in the forecasting model we have to reserve extra instance for the present.
        instance_offset = max(forecasting_mv_history_length + 1,
                              forecasting_cv_history_length + 1,
                              self.generative_model["model"].estimator_params["history_length"])

        realtime_df = realtime_df.tail(instance_offset)
        self.present_index = realtime_df.values.shape[0] - 1
        self.present_cutoff = self.present_index + 1
        self.present_offset = self.present_cutoff - instance_offset

        # This property is used for shifting the forecasting models input with the proper amount,
        # so that we can extend the end of the shift with the generated MV by the generation model.
        # We put '+1', because we have to shift one extra index for the present index for the forecasting
        # input.
        self.f_mv_input_shift = diff_lag + 1 if diff_lag < instance_offset else instance_offset

        self.minute_input = minutes[self.present_offset:self.present_cutoff]
        self.day_input = days[self.present_offset:self.present_cutoff]
        self.realtime_df = realtime_df

        self.tags = tags
        self.tags_to_index = tags_to_index

    def cvs_within_limit_checking(self, target_tags) -> Tuple[bool, str]:
        '''It returns true if the CV(s) are outside of the limits.'''
        within_limits = True
        data_sensor_tags = set(self.realtime_df.columns)

        limits = self.target_limits
        not_passed_tags = list()
        for tag in target_tags:
            if tag not in data_sensor_tags:
                continue
            value = self.realtime_df[tag][self.present_index]
            if tag in self.tags_to_index:
                condition_not_fullfilled = not limits[tag](value)
                if condition_not_fullfilled:
                    within_limits = False
                    not_passed_tags.append(tag)

        return (within_limits, not_passed_tags)

    def initial_constraint_checking(self) -> Tuple[bool, Dict[str, float]]:
        print('################# INITIAL CONSTRAINT CHECKING #################')

        not_all_conditions_fullfilled = False
        data_sensor_tags = set(self.realtime_df.columns)
        constrans_not_passing = dict()
        # For the initial constraints we are also checking if efficiency is suboptimal to begin with and
        # only if it is suboptimal we proceed further.
        const_limits = dict(self.const_limits)
        const_limits[config.EFFICIENCY_TAG] = lambda current_x, delta_x: 0 <= current_x + delta_x <= config.SUBOPTIMAL_EFFICIENCY_THRESHOLD

        for condition in const_limits.keys():
            if condition not in data_sensor_tags:
                continue
            value = self.realtime_df[condition][self.present_index]
            if condition in self.tags_to_index:
                condition_not_fullfilled = not const_limits[condition](value, 0)
                if condition_not_fullfilled:
                    not_all_conditions_fullfilled = True
                    constrans_not_passing[condition] = value

        return (not_all_conditions_fullfilled, constrans_not_passing)

    @ staticmethod
    def calculate_delta_mv(sensor_input, recommendation):
        delta_mv_dict = {}
        for col in recommendation.columns:
            delta_mv_dict[col] = [recommendation[col].values[0] - sensor_input[col].values[0]]

        return pd.DataFrame(delta_mv_dict)

    @ staticmethod
    def initial_flat_data_checking(realtime_df: pd.DataFrame) -> List[str]:
        print('################# INITIAL FLAT DATA CHECKING #################')

        values = realtime_df.values.astype(float)
        std_dev = values.std(axis=0)
        mean = values.mean(axis=0)
        data_flats = list(realtime_df.columns[(std_dev == 0.0) & (mean == 0.0)])

        return data_flats

    def mv_generation(self):
        print('################# MV GENERATION #################', flush=True)

        # Rewrite the mappers from tags to indices in the input array:
        self.generative_model["model"].estimator_params["tags_to_index"] = self.tags_to_index
        self.generative_input = self.realtime_df.iloc[self.present_offset:self.present_cutoff, :][self.tags].to_numpy(
            copy=True)

        # REALLY IMPORTANT STEP OF EMBEDDING THE TARGET EXCESS OXYGEN VALUE IN THE PRESETNT VALUE:
        for optimizer_key in self.optimizers.keys():
            self.generative_input = self.optimizers[optimizer_key].update_gen_input(self.generative_input)

        generative_input: Dict[str, np.ndarray] = {
            config.MINUTE_INPUT_KIND: self.minute_input,
            config.DAY_INPUT_KIND: self.day_input,
            config.SENSOR_INPUT_KIND: self.generative_input
        }

        self.generative_model["model"].estimator_params["output_variables"] = self.recommendation_mv_variables
        self.generative_model["model"].estimator_params["generative_prediction"] = True
        self.generative_model["model"].estimator_params["n_suggestions"] = config.GENERATION_COUNT
        self.generated_mvs = self.generative_model.predict(generative_input)

        self.all_mv_dependent_conditions_satisfied = np.full(
            (config.GENERATION_COUNT, ), True)
        self.efficieny_model_limits_satisfied = np.full(
            (config.GENERATION_COUNT, ), True)

        mv_dependent_constraints = self.mv_dependent_constraints

        for i in range(config.GENERATION_COUNT):
            # We are accessing the values at each generation, because we need a copy of the dataframe in which we migh replace values and
            # we don't want to replace tha values in the 'realtime_df'.
            self.generative_input = self.realtime_df.iloc[self.present_offset:self.present_cutoff, :][self.tags].to_numpy(
                copy=True)

            # Checking if MV-specific bias limits are satisfied before the generative input is changed.
            for j, condition in enumerate(self.recommendation_mv_variables):
                if condition not in self.const_limits:
                    continue
                constraint_is_not_within_limits = not self.const_limits[condition](
                    current_x=self.generative_input[-1, self.tags_to_index[condition]],
                    delta_x=self.generated_mvs[-1, j, i]
                )
                if constraint_is_not_within_limits:
                    self.all_mv_dependent_conditions_satisfied[i] = False
                    break

            # Checking MV-dependent constraints only if bias limits are passing
            if self.all_mv_dependent_conditions_satisfied[i] == True:
                for j, tag in enumerate(self.recommendation_mv_variables):
                    self.generative_input[-1, self.tags_to_index[tag]
                                          ] += self.generated_mvs[-1, j, i]

                # MV dependent constrains forecasting
                # Here we shift the input by one in order to predict what will be the resulting dependent constrains,
                # after the change in the MVs.
                generative_input: Dict[str, np.ndarray] = {
                    config.MINUTE_INPUT_KIND: self.minute_input,
                    config.DAY_INPUT_KIND: self.day_input,
                    config.SENSOR_INPUT_KIND: self.generative_input
                }

                self.generative_model["model"].estimator_params["output_variables"] = mv_dependent_constraints
                # We switch to making point predictions instead of generative predictions
                self.generative_model["model"].estimator_params["generative_prediction"] = False
                predicted_mv_dependent_constrains = self.generative_model.predict(
                    generative_input)

                for j, condition in enumerate(mv_dependent_constraints):
                    if condition not in self.const_limits:
                        continue
                    constraint_is_not_within_limits = not self.const_limits[condition](
                        current_x=self.generative_input[-1, self.tags_to_index[condition]],
                        delta_x=predicted_mv_dependent_constrains[-1, j])

                    if constraint_is_not_within_limits:
                        self.all_mv_dependent_conditions_satisfied[i] = False
                        break

    def rearrange_forecast_cv_input_data(self, forecast_model):
        forecasting_tags_to_index = forecast_model.estimator_params["tags_to_index"]
        recomedation_mvs_set = set(self.recommendation_mv_variables)
        rearranged_forecasting_input = np.full(
            (self.present_cutoff-self.present_offset, len(self.tags)), np.nan)

        present_mvs = dict()

        for tag in forecasting_tags_to_index.keys():

            rearranged_forecasting_input[:, forecasting_tags_to_index[tag]
                                         ] = self.realtime_df.iloc[self.present_offset:self.present_cutoff, :].loc[:, tag]

            if tag in recomedation_mvs_set:
                present_mvs[tag] = rearranged_forecasting_input[-1, forecasting_tags_to_index[tag]]

                rearranged_forecasting_input[:, forecasting_tags_to_index[tag]] = np.roll(
                    rearranged_forecasting_input[:, forecasting_tags_to_index[tag]],
                    self.f_mv_input_shift,
                    axis=0)

        return rearranged_forecasting_input, present_mvs

    def prepare_forecast_cv_input(self, forecasting_input, generated_mvs, present_mvs, forecast_model):
        forecasting_tags_to_index = forecast_model.estimator_params["tags_to_index"]
        for j, tag in enumerate(self.recommendation_mv_variables):
            # We divide the generated MV in order to spread the divided amount over the whole input.
            # We do so instead ofinputing the whole generated MV value in order to avoid too drastic change at one  minute and
            # this drastic change might have not been seen by the forecasting model.
            generated_mv_minute_wise = generated_mvs[j] / self.f_mv_input_shift
            present_mv = present_mvs[tag]

            for p in range(forecasting_input.shape[0] - self.f_mv_input_shift, forecasting_input.shape[0]):
                forecasting_input[p, forecasting_tags_to_index[tag]] = present_mv + generated_mv_minute_wise
                generated_mv_minute_wise += generated_mv_minute_wise

        # Prepare the input for prediction
        present_input = forecasting_input[np.newaxis, -1, :]

        sensor_input: Dict[str, np.ndarray] = {
            config.MINUTE_INPUT_KIND: self.minute_input,
            config.DAY_INPUT_KIND: self.day_input,
            # The present index is the last one.
            config.PRESENT_INDICES: [forecasting_input.shape[0] - 1],
            config.PRESENT_INPUT: present_input,
            config.SENSOR_INPUT_KIND: forecasting_input
        }

        return sensor_input

    def forecast_cv(self, forecast_pipeline, sensor_input):
        scaled_input = forecast_pipeline["preprocessing"].transform(sensor_input)
        model = forecast_pipeline["model"]
        # Check whether the recommended MVs are within the efficiency forecaster's capacity.
        model_limits_satisfied = model.is_input_within_limits(scaled_input)

        predictions = model.predict(scaled_input)

        return model_limits_satisfied, predictions

    def cv_forecasting(self):
        print('################# EFFICIENCY and EXCESS OXYGEN FORECASTING #################', flush=True)

        final_mv_recomendations = None
        final_cv_predictions = None
        any_efficiency_gain_detected = True

        # Rewrite the mappers from tags to indices in the input array:
        rearranged_forecasting_inputs = dict()
        present_model_mvs = dict()

        for model_name in self.forecasting_models.keys():
            rearranged_forecasting_input, present_mvs = self.rearrange_forecast_cv_input_data(
                self.forecasting_models[model_name]["model"])
            rearranged_forecasting_inputs[model_name] = rearranged_forecasting_input
            present_model_mvs[model_name] = present_mvs

        models_predictions = dict()
        models_limits_satisfied = dict()
        sensor_inputs = dict()

        current_state_total_reward = 0
        for cv_tag in self.target_variables:
            model_name = self.forecasting_model_names[cv_tag]
            self.optimizers[model_name].update_current_state_reward(self.realtime_df[cv_tag])
            current_state_total_reward += self.optimizers[model_name].current_state_reward

        current_state_total_reward /= len(self.target_variables)
        max_total_reward = float("-Inf")

        for i in range(config.GENERATION_COUNT):

            all_conditions_satisfied = True

            for model_name in rearranged_forecasting_inputs.keys():

                generated_mvs = self.generated_mvs[-1, :, i]
                # We are accessing the values at each generation,
                # because we need a copy of the dataframe in which we might replace values and
                # we don't want to replace tha values in the 'realtime_df'.
                sensor_input = self.prepare_forecast_cv_input(
                    rearranged_forecasting_inputs[model_name].copy(),
                    generated_mvs,
                    present_model_mvs[model_name],
                    self.forecasting_models[model_name]["model"])

                sensor_inputs[model_name] = sensor_input
                (model_limits_satisfied, predictions) = self.forecast_cv(
                    self.forecasting_models[model_name],
                    sensor_input)

                models_predictions[model_name] = predictions
                models_limits_satisfied[model_name] = model_limits_satisfied

                if model_limits_satisfied:
                    self.any_model_constraint_check_satisfied = True
                else:
                    all_conditions_satisfied = False

            forecast_within_limits = dict()
            total_reward = 0

# Estimating the target distance from desired change.
            for cv_tag in self.target_variables:
                forecasting_model_name = self.forecasting_model_names[cv_tag]

                forecasting_model = self.forecasting_models[forecasting_model_name]["model"]
                forecasting_present_input = sensor_inputs[forecasting_model_name][config.PRESENT_INPUT]
                forecasting_prediction = models_predictions[forecasting_model_name]
                optimizer = self.optimizers[forecasting_model_name]
                optimizer.calculate_temporal_reward(
                    forecasting_model,
                    forecasting_prediction,
                    forecasting_present_input)
                total_reward += optimizer.temporal_reward
                forecast_within_limits[cv_tag] = optimizer.is_target_within_limits()

            # Checking directional variables (EOs) related constraints
            total_result_dv = 0
            total_current_dv = 0
            total_sweet_spot = 0
            dv_limits_fulfilled = True
            for dv_tag in self.direction_variables:
                forecasting_model_name = self.forecasting_model_names[dv_tag]

                forecasting_model = self.forecasting_models[forecasting_model_name]["model"]
                forecasting_present_input = sensor_inputs[forecasting_model_name][config.PRESENT_INPUT]
                result_dv = models_predictions[forecasting_model_name]
                gen_input_index = self.generative_model["model"].estimator_params["tags_to_index"][dv_tag]
                current_dv = self.generative_input[-1, gen_input_index]
                sweet_spot = self.excess_oxy_sweet_spot[dv_tag]
                if not self.const_limits[dv_tag](current_dv, abs(result_dv-current_dv)):
                    dv_limits_fulfilled = False

                total_result_dv += result_dv
                total_current_dv += current_dv
                total_sweet_spot += sweet_spot

            # Could also be left as total w/o division.
            total_result_dv /= len(self.direction_variables)
            total_current_dv /= len(self.direction_variables)
            total_sweet_spot /= len(self.direction_variables)

            # Adding tolerance to the check of directions.
            dv_directions_satisfied = (
                ((abs(result_dv - sweet_spot) + config.TARGET_EXCESS_OXY_CONSERVATIVE_TOLERANCE) <=
                 (abs(current_dv - sweet_spot)))
                or (
                    (abs(result_dv - sweet_spot) - config.TARGET_EXCESS_OXY_CONSERVATIVE_TOLERANCE) <=
                    (abs(current_dv - sweet_spot))))

            # We average the min_excess_oxy_dist in order to be more interpretable
            total_reward /= len(self.target_variables)

            if dv_directions_satisfied and dv_limits_fulfilled:
                self.any_dv_constraints_check_satisfied = True
            else:
                all_conditions_satisfied = False

            if self.all_mv_dependent_conditions_satisfied[i]:
                self.any_mv_constraint_check_satisfied = True
            else:
                all_conditions_satisfied = False

            if total_reward < current_state_total_reward:
                all_conditions_satisfied = False

            # Assign to False if DV delta is too large
            if all_conditions_satisfied:

                efficiency_model_name = self.forecasting_model_names[config.EFFICIENCY_TAG]
                if efficiency_model_name in self.optimizers:
                    any_efficiency_gain_detected = False
                    eff_optimizer = self.optimizers[efficiency_model_name]
                    # TODO: replace the tolerance factor by distribution data form the eff forecaster.
                    if eff_optimizer.temporal_horizon_eff_diff > 0 - config.NEG_EFFICIENCY_TOLERANCE and \
                            eff_optimizer.temporal_horizon_eff_diff <= (config.TARGET_EFFICIENCY_DIFF * config.TARGET_EFFICIENCY_TOP_TOLERANCE_FACTOR):
                        any_efficiency_gain_detected = True

                if total_reward > max_total_reward and any_efficiency_gain_detected:
                    # If the reward is satisfying we accept the recommended values.
                    final_mv_recomendations = generated_mvs
                    final_cv_predictions = models_predictions
                    for optimizer_key in self.optimizers.keys():
                        self.optimizers[optimizer_key].update_optimal_reward()
                    max_total_reward = total_reward

                self.forecast_excess_oxy_within_limits = forecast_within_limits
        final_mv_recomendations_df = pd.DataFrame(
            final_mv_recomendations[np.newaxis, ...],
            columns=self.recommendation_mv_variables) if final_mv_recomendations is not None else None
        return final_mv_recomendations_df, final_cv_predictions, self.optimizers


class EfficiencyOptimizer:
    def __init__(self, forcast_input_index, gen_input_indices, limits, dv_sweet_spot):
        self.gen_input_indices = gen_input_indices
        self.dv_sweet_spot = dv_sweet_spot
        self.limits = limits
        self.forcast_input_index = forcast_input_index

    def update_gen_input(self, generative_input):
        for dv_tag, index in self.gen_input_indices.items():
            current_dv = generative_input[-1, index]
            generative_input[-1, index] += self.dv_sweet_spot[dv_tag] - current_dv
        return generative_input

    def update_optimal_reward(self):
        self.max_horizon_eff_diff = self.temporal_horizon_eff_diff

    def update_current_state_reward(self, state):
        # Staying in that state we have no diff change thus no reward.
        self.current_state_horizon_eff_diff = 0

    @ property
    def current_state_reward(self):
        """The reward before starting max reward search."""
        return self.current_state_horizon_eff_diff

    @ property
    def temporal_reward(self):
        return self.temporal_horizon_eff_diff

    @ property
    def optimal_reward(self):
        """The reward for efficiency optimizer is the positive difference between last and first value of the horizon.
                The temporal reward, while deciding whether this one is the best."""
        return self.max_horizon_eff_diff

    def calculate_temporal_reward(self, forecasting_model, predictions, present_input):
        last_prediction = predictions[0, 0, -1]
        if forecasting_model.estimator_params["label_lag"] > 0:
            self.temporal_horizon_eff_diff = last_prediction - \
                present_input[-1, self.forcast_input_index]
        else:
            self.temporal_horizon_eff_diff = last_prediction - predictions[0, 0, 0]

    def is_target_within_limits(self):
        return True


class OxygenOptimizer:
    def __init__(self, gen_input_index, sweet_spot, limits):
        self.gen_input_index = gen_input_index
        self.sweet_spot = sweet_spot
        self.limits = limits

    def update_gen_input(self, generative_input):
        current_excess_oxy = generative_input[-1, self.gen_input_index]
        # TODO: Add directional factor
        generative_input[-1, self.gen_input_index] += self.sweet_spot - current_excess_oxy

        return generative_input

    def update_optimal_reward(self):
        self.min_excess_oxy_dist = self.temporal_excess_oxy_dist

    def update_current_state_reward(self, state):
        self.current_state_excess_oxy_dist = abs(self.sweet_spot - state[-1])

    @ property
    def current_state_reward(self):
        return -self.current_state_excess_oxy_dist

    @ property
    def temporal_reward(self):
        return -self.temporal_excess_oxy_dist

    @ property
    def optimal_reward(self):
        """The reward for oxygen optimizer is the negative distance from the sweet spot."""
        return - self.min_excess_oxy_dist

    def calculate_temporal_reward(self, forecasting_model, predictions, present_input):
        last_prediction = predictions[0, 0, -1]
        self.prediction = last_prediction
        self.temporal_excess_oxy_dist = abs(last_prediction - self.sweet_spot)

    def is_target_within_limits(self):
        if self.limits(self.prediction - config.TARGET_EXCESS_OXY_CONSERVATIVE_TOLERANCE) and \
                self.limits(self.prediction + config.TARGET_EXCESS_OXY_CONSERVATIVE_TOLERANCE):
            return True

        return False


def extract_day_min_from_date(date):
    delta = date - config.DATE_OFFSET

    day = delta.days
    minute = (date.hour * 60) + date.minute

    return day, minute


def get_from_disk_realtime_data(
        random_num,
        unit: str,
        table_name: str,
        timesteps: int,
        contract: TagContract,
        model_converters: Dict[str, Any],
        model_converter_input_tags: Dict[str, Any]):
    """This function is ubeing used just for deriving the data from the local disk instead from the database.
       Thus it should have the same output as 'get_realtime_data' and we do NOT deploy this function, because
       it is onlt for local use."""

    original_dataset = pd.read_csv(f"data/prepared_datasets/{unit}/{table_name}", parse_dates=[
        config.REAL_TIME_DATE_COLUMN_NAME], index_col=config.REAL_TIME_DATE_COLUMN_NAME)
    # original_dataset.index = original_dataset.index.dt.floor('Min')
    original_dataset = original_dataset.resample("1min").mean().interpolate()

    if random_num is None:
        original_dataset = original_dataset.tail(timesteps)
    else:
        # FOR TESTING PURPOSE!
        original_dataset = original_dataset.iloc[int(random_num):]
        original_dataset = original_dataset.head(timesteps)

    dataset, original_dataset = convert_dataset(contract=contract, dataset=original_dataset)

    return dataset, original_dataset


def detect_and_label_outliers_as_nan(data, m=2):
    for column in data.columns:
        d = np.abs(data[column] - np.mean(data[column]))
        mdev = np.mean(d)
        s = d/mdev if mdev else np.zeros(d.shape)
        data.loc[s > m, column] = np.NaN

    return data


def get_realtime_data(
        unit: str,
        engine,
        table_name: str,
        sensor_tags,
        timesteps: int,
        contract: TagContract,
        model_converters: Dict[str, Any],
        model_converter_input_tags: Dict[str, Any]):
    """This function is ubeing used just for deriving the data from the local disk instead from the database.
       Thus it should have the same output as 'get_realtime_data' and we do NOT deploy this function, because
       it is onlt for local use."""

    # q = f"""SELECT * FROM {table_name} WHERE tag_name IN {tuple(sensor_tags)} AND ts >= NOW() - INTERVAL {timesteps} MINUTE ORDER BY ts DESC"""
    q = f"""SELECT tag_name, ts, value FROM tb_combustion_model_input A WHERE tag_name IN {tuple(sensor_tags)} AND ts >= NOW() - INTERVAL {timesteps} MINUTE
            UNION
            SELECT B.f_address_no, B.f_date_rec, B.f_value FROM tb_bat_history B WHERE f_address_no IN {tuple(sensor_tags)} AND f_updated_at >= NOW() - INTERVAL {timesteps} MINUTE
            ORDER BY ts DESC"""

    dataset = pd.read_sql(q, con=engine)

    # realtime data preprocessing
    dataset["value"] = pd.to_numeric(dataset["value"])
    dataset[config.REAL_TIME_DATE_COLUMN_NAME] = pd.to_datetime(dataset["ts"])
    original_dataset = pd.pivot_table(
        dataset, values='value', index=config.REAL_TIME_DATE_COLUMN_NAME, columns='tag_name')
    original_dataset = original_dataset.resample("1min").mean().interpolate()

    # for t in sensor_tags:
    #     if t not in original_dataset.columns:
    #         original_dataset[t] = 0

    # sensor tags converter
    converters = pd.read_csv('data/sensor_tag_converters.csv')
    converters = converters[converters['unit'] == unit].set_index('tag_opc')['tag_dcs']
    tags_not_found = []
    for t in converters.index:
        if converters[t] in original_dataset.columns:
            original_dataset[t] = original_dataset[converters[t]]
        else:
            tags_not_found.append(converters[t])
    if len(tags_not_found) > 0:
        raise ValueError(f"These tags are not found on original_datasets: {tags_not_found}")

    dataset, original_dataset = convert_dataset(contract=contract, dataset=original_dataset)

    return dataset, original_dataset


def get_realtime_data_old(
    engine,
    tb_name,
    sensors,
    alias_sensors,
    sensor_list_df,
    n_minutes,
    contract,
    model_converters,
        model_converter_input_tags):

    print('################# FUNCTION FOR GETTING REALTIME DATA #################')
    if len(alias_sensors) > 1:
        where_script = f'in {str(tuple(alias_sensors))}'
    else:
        where_script = f'="{alias_sensors[0]}"'

    query = f'SELECT * FROM {tb_name} WHERE tag_name {where_script} AND ts >= NOW() - INTERVAL {n_minutes} MINUTE ORDER BY ts DESC'
    dataset = pd.read_sql(query, con=engine)

    # realtime data preprocessing
    dataset["value"] = pd.to_numeric(dataset["value"])
    dataset[config.REAL_TIME_DATE_COLUMN_NAME] = pd.to_datetime(dataset["ts"])
    pivoted_dataset = pd.pivot_table(
        dataset, values='value', index=config.REAL_TIME_DATE_COLUMN_NAME, columns='tag_name')
    pivoted_dataset = pivoted_dataset.resample("1min").mean().interpolate()

    # TODO: Need to clarify this!
    for sensor in alias_sensors:
        if not sensor in pivoted_dataset.columns:
            pivoted_dataset[sensor] = np.nan

    return convert_dataset(
        contract=contract,
        dataset=pivoted_dataset,
        model_converters=model_converters,
        model_converters_association_tags=model_converter_input_tags)


def get_latest_id(engine, table, col):
    query = f'SELECT MAX({col}) FROM {table}'
    result = pd.read_sql(query, con=engine)

    return result.values[0][0]


def get_mit_config(engine, table):
    dbname = table.split('.')[0]
    query = f'''SELECT tag_dcs as tag, description, f_value as value FROM {table} tcct
                LEFT JOIN {dbname}.tb_bat_raw tcr
                ON tcct.tag_name = tcr.f_address_no
                WHERE description LIKE "%%Inlet Temp%%"'''
    result = pd.read_sql(query, con=engine)

    query = f'''SELECT tag_dcs as tag, description, f_value as value FROM {table} tcct
                LEFT JOIN {dbname}.tb_bat_raw tcr
                ON tcct.tag_name = tcr.f_address_no
                WHERE description LIKE "%%Outlet Temp%%"
                GROUP BY description'''
    result2 = pd.read_sql(query, con=engine)
    return result, result2


def noise_reduction(y: np.ndarray, days: np.ndarray, wavelet="db4"):
    results = np.empty(y.shape)

    sucessive_count = 0
    if days.shape[0] > 0:
        prev_day = days[0]
        sucessive_count = 1

    successive_counts = list()

    for i in range(1, days.shape[0]):
        current_day = days[i]
        if(current_day == prev_day):
            sucessive_count += 1
        else:
            successive_counts.append(sucessive_count)
            sucessive_count = 1

        prev_day = current_day
    if sucessive_count > 0:
        successive_counts.append(sucessive_count)

    offset = 0
    for sucessive_count in successive_counts:
        signal = y[offset:offset+sucessive_count]

        coeff = pywt.wavedec(signal, wavelet, mode="per")

        sigma = (1/0.6745) * \
            np.mean(np.absolute(coeff[-1] - np.mean(coeff[-1])))
        # Calculte the univeral threshold
        s_thresh = sigma * np.sqrt(2*np.log(signal.shape[0]))

        coeff[1:] = (pywt.threshold(i, value=s_thresh, mode="soft")
                     for i in coeff[1:])

        signal = pywt.waverec(coeff, wavelet, mode="per")
        results[offset:offset+sucessive_count] = signal[:y[offset:offset +
                                                           sucessive_count].shape[0]]
        offset += sucessive_count

    # Sometimes when we have near no variation we get NAs and in that case we return back
    # the original data.
    if np.isnan(results).sum() > np.isnan(y).sum():
        results = y

    return results

# use REPLACE INTO commands instead of INSERT INTO


def mysql_replace_into(table, conn, keys, data_iter):
    from sqlalchemy.dialects.mysql import insert
    from sqlalchemy.ext.compiler import compiles
    from sqlalchemy.sql.expression import Insert

    @ compiles(Insert)
    def replace_string(insert, compiler, **kw):
        s = compiler.visit_insert(insert, **kw)
        s = s.replace("INSERT INTO", "REPLACE INTO")
        return s

    data = [dict(zip(keys, row)) for row in data_iter]

    conn.execute(table.table.insert(replace_string=""), data)
