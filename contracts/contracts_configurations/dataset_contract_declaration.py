import sys

if ".." not in sys.path:
    sys.path.append("..")

try:
    from tag_contract_factory import DatasetTagAssociationKind
except ImportError:
    from ..tag_contract_factory import DatasetTagAssociationKind


TAG_TO_ASSOCIATION_KIND={

    # Custom Formula associations

    "Hot Reheat Steam Flow": DatasetTagAssociationKind.CustomFormulaPriorMapping,
    "Coal HHV": DatasetTagAssociationKind.CustomFormulaPriorMapping,
    # Total Secondary Airflow has to be placed first in the group of CustomFormulaPostMapping,
    # because the calculation of the later tags are dependent on it value.
    "Total Secondary Air Flow": DatasetTagAssociationKind.CustomFormulaPostMapping,
    "Efficiency": DatasetTagAssociationKind.CustomFormulaPostMapping,
    "SA to PA Ratio": DatasetTagAssociationKind.CustomFormulaPostMapping,
    "Air to Fuel Ratio": DatasetTagAssociationKind.CustomFormulaPostMapping,

    # Multiple associations

    "Total Primary Air Flow": DatasetTagAssociationKind.MultipleSum,
    "SH Spray Water Flow": DatasetTagAssociationKind.MultipleSum,
    "Reheat Spray Water Flow": DatasetTagAssociationKind.MultipleSum,
    "Coal Flow": DatasetTagAssociationKind.MultipleSum,
    "SA Fan A": DatasetTagAssociationKind.MultipleSum,
    "SA Fan B": DatasetTagAssociationKind.MultipleSum,

    "Feed Water Flow": DatasetTagAssociationKind.MultipleAverage,
    "Main Steam Pressure": DatasetTagAssociationKind.MultipleAverage,
    "Hot Reheat Steam Pressure": DatasetTagAssociationKind.MultipleAverage,
    "Hot Reheat Steam Temperature": DatasetTagAssociationKind.MultipleAverage,
    "Mill A Outlet Temperature": DatasetTagAssociationKind.MultipleAverage,
    "Mill B Outlet Temperature": DatasetTagAssociationKind.MultipleAverage,
    "Mill C Outlet Temperature": DatasetTagAssociationKind.MultipleAverage,
    "Mill D Outlet Temperature": DatasetTagAssociationKind.MultipleAverage,
    "Mill E Outlet Temperature": DatasetTagAssociationKind.MultipleAverage,
    "Windbox-to-Furnace Pressure": DatasetTagAssociationKind.MultipleAverage,
    "Furnace Pressure A": DatasetTagAssociationKind.MultipleAverage,
    "Furnace Pressure B": DatasetTagAssociationKind.MultipleAverage,
    "Furnace Pressure C": DatasetTagAssociationKind.MultipleAverage,
    "Steam Drum Pressure": DatasetTagAssociationKind.MultipleAverage,
    "Bed Pressure": DatasetTagAssociationKind.MultipleAverage,
    "Bed Temperature": DatasetTagAssociationKind.MultipleAverage,

    # Single Associations

    "ECONOMIZER GAS OUTLET A OXYGEN #1": DatasetTagAssociationKind.Single,
    "ECONOMIZER GAS OUTLET B OXYGEN #1": DatasetTagAssociationKind.Single,

    "Burner Tilt Position 0": DatasetTagAssociationKind.Single,
    "Burner Tilt Position 1": DatasetTagAssociationKind.Single,
    "Burner Tilt Position 2": DatasetTagAssociationKind.Single,
    "Burner Tilt Position 3": DatasetTagAssociationKind.Single,

    "Cold Reheat Steam Pressure": DatasetTagAssociationKind.Single,
    "SH Spray Water Pressure": DatasetTagAssociationKind.Single,
    "Feed Water Pressure": DatasetTagAssociationKind.Single,
    "Main Steam Temperature": DatasetTagAssociationKind.Single,
    "Cold Reheat Steam Temperature": DatasetTagAssociationKind.Single,
    "SH Spray Temperature": DatasetTagAssociationKind.Single,
    "Feed Water Temperature": DatasetTagAssociationKind.Single,
    "Main Steam Flow": DatasetTagAssociationKind.Single,
    "Generator Gross Megawatt": DatasetTagAssociationKind.Single,
    "Bias Burner Tilt Position": DatasetTagAssociationKind.Single,

    # "FURNACE PRESSURE HIGH": DatasetTagAssociationKind.Single,
    # "FURNACE PRESSURE HIGH ALARM": DatasetTagAssociationKind.Single,
    # "FURNACE PRESSURE LOW": DatasetTagAssociationKind.Single,
    # "FURNACE PRESSURE LOW ALARM": DatasetTagAssociationKind.Single,

    "No.1 FD FAN MOVABLE BLADE  CTL": DatasetTagAssociationKind.Single,
    "No.1 FD FAN MOVABLE BLADE  POSI": DatasetTagAssociationKind.Single,
    "No1 FD FAN MOT CURRENT": DatasetTagAssociationKind.Single,
    "No.2 FD FAN MOVABLE BLADE CTL": DatasetTagAssociationKind.Single,
    "No.2 FD FAN MOVABLE BLADE POSI": DatasetTagAssociationKind.Single,
    "No2 FD FAN MOT CURRENT": DatasetTagAssociationKind.Single,

    "Opening angle of control valve at secondary air fan 1 inlet": DatasetTagAssociationKind.Single,
    "Opening angle of control valve at secondary air fan 2 inlet": DatasetTagAssociationKind.Single,
    "O2 CT at Low TEM SHTR OUTL (LE)": DatasetTagAssociationKind.Single,
    "O2 CT at Low TEM SHTR OUTL (RI)": DatasetTagAssociationKind.Single
}

TAG_ACTIONS={
    "CONST_LIMITS",
    "TARGET_LIMITS",
    "MODELLING_CONSTRAINT_FILTER",
    "GET_EXCESS_OXYGEN_SWEET_SPOT",
    "RESCALE_SENSOR"
}

TAG_GROUPS={
    "PRESSURE_TAGS",
    "MODELLING_CONSTRAINT_VARIABLES",
    "FLOW_TAGS",
    "RECOMMENDATION_MV_VARIABLES",
    "RECOMMENDATION_TARGET_VARIABLES",
    "RECOMMENDATION_DIRECTION_VARIABLES",
    "DV_VARIABLES",
    "MILL_OUTLET_CONSTRAINTS",
    "MV_DEPENDENT_CONSTRAINTS",
    "IGNORED_TAGS",
    "MODELLED_TAGS"
}