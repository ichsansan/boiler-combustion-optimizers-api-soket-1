import numpy as np
import sys

if "../../actions" not in sys.path:
    sys.path.append("../../actions")

from actions.dataset_contract_action_implementations import calculate_total_rh_flow_taa1, calculate_efficiency

TAG_ASSOCIATIONS = {

    # Custom Formula associations

    "Total Secondary Air Flow": lambda df_before, df_after: df_before["DISP1ALLWIND_PNT"].values - df_after["Total Primary Air Flow"].values,
    "Coal HHV": lambda df_before: np.full(np.array([df_before.shape[0], 1]), 4100),
    "Hot Reheat Steam Flow": calculate_total_rh_flow_taa1,
    "Efficiency": {
        'EWS102/10DAS0B:AI1B022601.PNT': None,
        'EWS102/10DAS0B:RT1B023501.PNT': None,
        'EWS102/10DAS0B:RT1B023601.PNT': None,
        'EWS102/10DAS0B:TC1B023102.PNT': None,
        'EWS102/10DAS0B:TC1B023103.PNT': None,
        'EWS102/10DAS0B:TC1B023202.PNT': None,
        'EWS102/10DAS0B:ZT1B020706.PNT': None,
        'EWS102/10DAS0B:ZT1B021105.PNT': None,
        'EWS102/10DAS0B:ZT1B021206.PNT': None,
        'EWS102/10DAS0B:ZT1B021307.PNT': None,
        'EWS102/10DAS0B:ZT1B021406.PNT': None,
        'EWS102/10DAS0B:ZT1B021606.PNT': None,
        'EWS102/10DAS0D:RT1D012706.PNT': None,
        'EWS102/10DAS0D:RT1D026404.PNT': None,
        'EWS102/10DAS0D:RT1D026501.PNT': None,
        'EWS102/10DAS0D:TC1D027201.PNT': None,
        'EWS102/10DAS0E:RT1E032706.PNT': None,
        'EWS102/10DAS0E:RT1E046504.PNT': None,
        'EWS102/10DAS0E:RT1E046506.PNT': None,
        'EWS102/10DAS0E:RT1E046601.PNT': None,
        'EWS102/10DAS0E:TC1E047104.PNT': None,
        'EWS102/10DISP:OXY.PNT': None,
        'EWS102/10DISP:STEAMFLOWB.PNT': None,
        'EWS102/10MFLOW:AIN1.PNT': None,
        'EWS102/10MFLOW:AIN10.PNT': None,
        'EWS102/10MFLOW:AIN11.PNT': None,
        'EWS102/10MFLOW:AIN12.PNT': None,
        'EWS102/10MFLOW:AIN2.PNT': None,
        'EWS102/10MFLOW:AIN3.PNT': None,
        'EWS102/10MFLOW:AIN4.PNT': None,
        'EWS102/10MFLOW:AIN5.PNT': None,
        'EWS102/10MFLOW:AIN6.PNT': None,
        'EWS102/10MFLOW:AIN7.PNT': None,
        'EWS102/10MFLOW:AIN8.PNT': None,
        'EWS102/10MFLOW:AIN9.PNT': None
    },
    "SA to PA Ratio": lambda df_before, df_after:np.divide(df_after["Total Secondary Air Flow"].values, df_after["Total Primary Air Flow"].values, out=np.zeros_like(df_after["Total Secondary Air Flow"].values), where=df_after["Total Primary Air Flow"].values!=0),
    "Air to Fuel Ratio": lambda df_before, df_after: np.divide(df_after["Total Secondary Air Flow"].values + df_after["Total Primary Air Flow"].values, df_after["Coal Flow"].values, out=np.zeros_like(df_after["Total Primary Air Flow"].values), where=df_after["Coal Flow"].values!=0),

   # Multiple associations

    "Total Primary Air Flow": {
        "MFLOW1AIN3_PNT": lambda x: x * 0.5,
        "MFLOW1AIN4_PNT": lambda x: x * 0.5,
        "MFLOW1AIN1_PNT": lambda x: x * 0.5,
        "MFLOW1AIN2_PNT": lambda x: x * 0.5,
        "MFLOW1AIN5_PNT": lambda x: x * 0.5,
        "MFLOW1AIN6_PNT": lambda x: x * 0.5,
        "MFLOW1AIN7_PNT": lambda x: x * 0.5,
        "MFLOW1AIN8_PNT": lambda x: x * 0.5,
        "MFLOW1AIN11_PNT": lambda x: x * 0.5,
        "MFLOW1AIN12_PNT": lambda x: x * 0.5
    },
    "Feed Water Flow": {
        "FT1A010105_PNT": None,
        "FT1A010205_PNT": None,
        "FT1A010305_PNT": None
    },
    "SH Spray Water Flow": {
        "FT1A011204_PNT": None,
        "FT1A010601_PNT": None
    },
    "Reheat Spray Water Flow": {
        "FT1A010405_PNT": None,
        "FT1A011206_PNT": None
    },
    "Coal Flow": {
        "ZT1B021406_PNT": None,
        "ZT1B020706_PNT": None,
        "ZT1B021105_PNT": None,
        "ZT1B021206_PNT": None,
        "ZT1B021307_PNT": None,
        "ZT1B021606_PNT": None,
    },

    "Main Steam Pressure": {
        "PT1J010501_PNT": None,
        "PT1J010601_PNT": None,
        "PT1J010704_PNT": None,
    },
    "Hot Reheat Steam Pressure": {
        "PT1A010404_PNT": None,
        "PT1A010506_PNT": None,
    },
    "Hot Reheat Steam Temperature": {
        "TC1A012204_PNT": None,
        "TC1M053202_PNT": None,
        "TC1A012205_PNT": None,
        "TC1A012305_PNT": None,
    },
    "Mill A Outlet Temperature": {
        "RT1B023401_PNT": None,
        "RT1B023502_PNT": None,
        "RT1B023602_PNT": None,
        "RT1F113301_PNT": None,
        "RT1F113401_PNT": None,
        "RT1F113501_PNT": None
    },
    "Mill B Outlet Temperature": {
        "RT1B023402_PNT": None,
        "RT1B023503_PNT": None,
        "RT1B023603_PNT": None,
        "RT1F124301_PNT": None,
        "RT1F124401_PNT": None,
        "RT1F124501_PNT": None
    },
    "Mill C Outlet Temperature": {
        "RT1B023403_PNT": None,
        "RT1B023504_PNT": None,
        "RT1B023604_PNT": None,
        "RT1G133301_PNT": None,
        "RT1G133401_PNT": None,
        "RT1G133501_PNT": None
    },
    "Mill D Outlet Temperature": {
        "RT1B023404_PNT": None,
        "RT1B023505_PNT": None,
        "RT1B023605_PNT": None,
        "RT1G144301_PNT": None,
        "RT1G144401_PNT": None,
        "RT1G144501_PNT": None,
    },
    "Mill E Outlet Temperature": {
        "RT1B023405_PNT": None,
        "RT1B023506_PNT": None,
        "RT1B023606_PNT": None,
        "RT1H153301_PNT": None,
        "RT1H153401_PNT": None,
        "RT1H153501_PNT": None,
    },
    "Windbox-to-Furnace Pressure": {
        "PT1C030706_PNT": None,
        "PT1C030805_PNT": None,
        "PT1C030407_PNT": None,
        "PT1I172202_PNT": None,
        "PT1C030506_PNT": None,
        "PT1C031101_PNT": None,
        "PT1C030606_PNT": None,
    },
    "Furnace Pressure A": {
        "PT1B020701_PNT": lambda x: x,
    },
    "Furnace Pressure B": {
        "PT1B021101_PNT": lambda x: x
    },
    "Furnace Pressure C": {
        "PT1B021201_PNT": lambda x: x,
        "PT1B021306_PNT": lambda x: x,
    },

    "Steam Drum Pressure": None,

    "Bed Pressure": None,

    "Bed Temperature": None,

    "SA Fan A": None,

    "SA Fan B": None,

    # Single Associations

    "ECONOMIZER GAS OUTLET A OXYGEN #1": {
        "AI1B022107_PNT": None
    },
    "ECONOMIZER GAS OUTLET B OXYGEN #1": {
        "AI1B022501_PNT": None
    },

    "Burner Tilt Position 0": {
        "ZT1C030101_PNT": None,
    },
    "Burner Tilt Position 1": {
        "ZT1C030101_PNT": None,
    },
    "Burner Tilt Position 2": {
        "ZT1C030101_PNT": None,
    },
    "Burner Tilt Position 3": {
        "ZT1C030101_PNT": None,
    },
    "Cold Reheat Steam Pressure": {
        "PT1J010805_PNT":  None,
    },
    "SH Spray Water Pressure": {
        "PT1B021607_PNT":  None,
    },
    "Feed Water Pressure": {
        "PT1J012501_PNT":  None,
    },
    "Main Steam Temperature": {
        "TC1M053102_PNT": None
    },
    "Cold Reheat Steam Temperature": {
        "TC1M053205_PNT": None
    },
    "SH Spray Temperature": {
        "RT1A013602_PNT": None
    },
    "Feed Water Temperature": {
        "TC1M053206_PNT": None
    },
    "Main Steam Flow": {
        "DISP1STEAMFLOWB_PNT":  None,
    },
    "Generator Gross Megawatt": {
        "AI1O013305_PNT": None
    },
    "Bias Burner Tilt Position": {
        "ZT1C030101_PNT": None
    },

    #"FURNACE PRESSURE HIGH": {
    #    "DI1I170101_CIN": None
    #},
    #"FURNACE PRESSURE HIGH ALARM": {
    #    "DI1I172502_CIN": None
    #},
    #"FURNACE PRESSURE LOW": {
    #    "DI1I170102_CIN": None
    #},
    #"FURNACE PRESSURE LOW ALARM": {
    #    "DI1I173401_CIN": None
    #},

    "No.1 FD FAN MOVABLE BLADE  CTL": None,
    "No.1 FD FAN MOVABLE BLADE  POSI": {
        "ZT1B020506_PNT": None
    },
    "No1 FD FAN MOT CURRENT": {
        "CT1D012607_PNT": None
    },
    "No.2 FD FAN MOVABLE BLADE CTL": None,
    "No.2 FD FAN MOVABLE BLADE POSI": {
        "ZT1B021605_PNT": None
    },
    "No2 FD FAN MOT CURRENT": {
        "CT1E046302_PNT": None
    },

    "Opening angle of control valve at secondary air fan 1 inlet": None,
    "Opening angle of control valve at secondary air fan 2 inlet": None,
    "O2 CT at Low TEM SHTR OUTL (LE)": None,
    "O2 CT at Low TEM SHTR OUTL (RI)": None
}

TAGS_TO_ACTIONS= {
    "CONST_LIMITS": {
        "Mill A Outlet Temperature": lambda current_x, delta_x: 45 <= current_x + delta_x <= 70,
        "Mill B Outlet Temperature": lambda current_x, delta_x: 45 <= current_x + delta_x <= 70,
        "Mill C Outlet Temperature": lambda current_x, delta_x: 45 <= current_x + delta_x <= 70,
        "Mill D Outlet Temperature": lambda current_x, delta_x: 45 <= current_x + delta_x <= 70,
        "Mill E Outlet Temperature": lambda current_x, delta_x: 45 <= current_x + delta_x <= 70,
        "Windbox-to-Furnace Pressure": lambda current_x, delta_x: current_x + delta_x <= 2.2,
        "Furnace Pressure A": lambda current_x, delta_x: -100 <= current_x + delta_x <= -5,
        "Furnace Pressure B": lambda current_x, delta_x: -100 <= current_x + delta_x <= -5,
        "Furnace Pressure C": lambda current_x, delta_x: -100 <= current_x + delta_x <= -5,
        "ECONOMIZER GAS OUTLET A OXYGEN #1": lambda current_x, delta_x: (0.5 <= current_x + delta_x <= 100) & (delta_x <= 0.5),
        "ECONOMIZER GAS OUTLET B OXYGEN #1": lambda current_x, delta_x: (0.5 <= current_x + delta_x <= 100) & (delta_x <= 0.5),
        "Total Secondary Air Flow": lambda current_x, delta_x: abs(delta_x) <= 0.15 * current_x,
        "Generator Gross Megawatt": lambda current_x, delta_x: current_x + delta_x >= 175 
    },
    "MODELLING_CONSTRAINT_FILTER": {
        # These constraints are based on thresholds for operationability of the boiler and concern training data
        # filtering and used only if they are in MODELLING_CONSTRAINT_VARIABLES Tag Group Mapping
        "Generator Gross Megawatt": lambda x: x >= 250
    },
    "RESCALE_SENSOR":{
        "Feed Water Flow": lambda x, scale_back=False: x * 3.6  if scale_back else x * 0.27777777777,
        "SH Spray Water Flow": lambda x, scale_back=False: x * 3.6  if scale_back else x * 0.27777777777,
        "Coal Flow": lambda x, scale_back=False: x * 3.6  if scale_back else x * 0.27777777777,
        "Main Steam Flow": lambda x, scale_back=False: x * 3.6  if scale_back else x * 0.27777777777,
        "Main Steam Pressure": lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "SH Spray Water Pressure": lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "Hot Reheat Steam Pressure": lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "Feed Water Pressure": lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "Cold Reheat Steam Pressure":lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "Furnace Pressure A": lambda x, scale_back=False: x * 9.80665 if scale_back else x * 0.10197,
        "Furnace Pressure B": lambda x, scale_back=False: x * 9.80665 if scale_back else x * 0.10197,
        "Furnace Pressure C": lambda x, scale_back=False: x * 9.80665 if scale_back else x * 0.10197
    },
    "TARGET_LIMITS": {
        "Efficiency": lambda x: x > 95,
        },

    "GET_EXCESS_OXYGEN_SWEET_SPOT": {
        "ECONOMIZER GAS OUTLET A OXYGEN #1": 3,
        "ECONOMIZER GAS OUTLET B OXYGEN #1": 3
        }
}

TAG_GROUP_MAP={
    "PRESSURE_TAGS": {
        "Main Steam Pressure",
        "Feed Water Pressure",
        "SH Spray Water Pressure",
        "Hot Reheat Steam Pressure",
        "Cold Reheat Steam Pressure"
    },
    "FLOW_TAGS":{
        "Coal Flow",
        "Main Steam Flow",
        "Feed Water Flow",
        "SH Spray Water Flow"
    },
    "RECOMMENDATION_MV_VARIABLES":{
        "Total Secondary Air Flow",
        "Burner Tilt Position 0",
        "Burner Tilt Position 1",
        "Burner Tilt Position 2",
        "Burner Tilt Position 3"
    },
    "RECOMMENDATION_TARGET_VARIABLES":{
        "Efficiency"
    },

    "MODELLING_CONSTRAINT_VARIABLES":{
        "Generator Gross Megawatt"
    },
    "RECOMMENDATION_DIRECTION_VARIABLES":{
        "ECONOMIZER GAS OUTLET A OXYGEN #1",
        "ECONOMIZER GAS OUTLET B OXYGEN #1"
    },

    "DV_VARIABLES":{
        "Coal HHV",
        "Coal Flow",
        "Total Primary Air Flow",
        "Generator Gross Megawatt",
        "No.1 FD FAN MOVABLE BLADE  POSI",
        "No.2 FD FAN MOVABLE BLADE POSI",
        },
    "MILL_OUTLET_CONSTRAINTS":{
        "Mill A Outlet Temperature",
        "Mill B Outlet Temperature",
        "Mill C Outlet Temperature",
        "Mill D Outlet Temperature",
        "Mill E Outlet Temperature"
    },
    "MV_DEPENDENT_CONSTRAINTS":{
        "Windbox-to-Furnace Pressure",
        "Furnace Pressure A",
        "Furnace Pressure B",
        "Furnace Pressure C",
        #"FURNACE PRESSURE HIGH",
        #"FURNACE PRESSURE HIGH ALARM",
        #"FURNACE PRESSURE LOW",
        #"FURNACE PRESSURE LOW ALARM"
    },
    "MODELLED_TAGS": {
        "Efficiency"
    },
    "IGNORED_TAGS": {
        "Steam Drum Pressure",
        "Opening angle of control valve at secondary air fan 1 inlet",
        "Opening angle of control valve at secondary air fan 2 inlet",
        "O2 CT at Low TEM SHTR OUTL (LE)",
        "O2 CT at Low TEM SHTR OUTL (RI)",
        "No.1 FD FAN MOVABLE BLADE  CTL",
        "No.2 FD FAN MOVABLE BLADE CTL",
        "SA Fan A",
        "SA Fan B",
        "Bed Pressure",
        "Bed Temperature",
    }
}
