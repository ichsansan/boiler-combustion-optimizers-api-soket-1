import numpy as np
import sys

if "../../actions" not in sys.path:
    sys.path.append("../../actions")

from actions.dataset_contract_action_implementations import calculate_total_rh_flow_taa2, calculate_efficiency

from config import EFFICIENCY_CAP, SUBOPTIMAL_EFFICIENCY_THRESHOLD

TAG_ASSOCIATIONS = {

    # Custom Formula associations

    "Total Secondary Air Flow": lambda df_before, df_after: df_before["DISP2ALLWIND_PNT"].values - df_after["Total Primary Air Flow"].values,
    "Coal HHV": lambda df_before: np.full(np.array([df_before.shape[0], 1]), 4100),
    "Hot Reheat Steam Flow": calculate_total_rh_flow_taa2,
    "Efficiency": {
       'EWS202/20DAS0A:FT2A010105.PNT': None,
       'EWS202/20DAS0A:FT2A010401.PNT': None,
       'EWS202/20DAS0A:FT2A010405.PNT': None,
       'EWS202/20DAS0A:FT2A010503.PNT': None,
       'EWS202/20DAS0A:FT2A010601.PNT': None,
       'EWS202/20DAS0A:FT2A011204.PNT': None,
       'EWS202/20DAS0A:FT2A011206.PNT': None,
       'EWS202/20DAS0A:PT2A010203.PNT': None,
       'EWS202/20DAS0A:PT2A010502.PNT': None,
       'EWS202/20DAS0A:RT2A013601.PNT': None,
       'EWS202/20DAS0A:RT2A013602.PNT': None,
       'EWS202/20DAS0A:TC2A011401.PNT': None,
       'EWS202/20DAS0B:AI2B022207.PNT': None,
       'EWS202/20DAS0B:AI2B022501.PNT': None,
       'EWS202/20DAS0B:AI2B022601.PNT': None,
       'EWS202/20DAS0B:PT2B020301.PNT': None,
       'EWS202/20DAS0B:PT2B021607.PNT': None,
       'EWS202/20DAS0B:TC2B023101.PNT': None,
       'EWS202/20DAS0B:TC2B023102.PNT': None,
       'EWS202/20DAS0B:TC2B023103.PNT': None,
       'EWS202/20DAS0B:TC2B023201.PNT': None,
       'EWS202/20DAS0B:TC2B023202.PNT': None,
       'EWS202/20DAS0B:ZT2B020706.PNT': None,
       'EWS202/20DAS0B:ZT2B021105.PNT': None,
       'EWS202/20DAS0B:ZT2B021206.PNT': None,
       'EWS202/20DAS0B:ZT2B021307.PNT': None,
       'EWS202/20DAS0B:ZT2B021406.PNT': None,
       'EWS202/20DAS0B:ZT2B021606.PNT': None,
       'EWS202/20DAS0D:RT2D012706.PNT': None,
       'EWS202/20DAS0D:RT2D026405.PNT': None,
       'EWS202/20DAS0D:TC2D027201.PNT': None,
       'EWS202/20DAS0E:RT2E032706.PNT': None,
       'EWS202/20DAS0E:RT2E046505.PNT': None,
       'EWS202/20DAS0E:TC2E047104.PNT': None,
       'EWS202/20DAS0J:PT2J010704.PNT': None,
       'EWS202/20DAS0J:PT2J010804.PNT': None,
       'EWS202/20DAS0J:PT2J010805.PNT': None,
       'EWS202/20DAS0J:PT2J012501.PNT': None,
       'EWS202/20DAS0K:PT2K026402.PNT': None,
       'EWS202/20DAS0K:PT2K026403.PNT': None,
       'EWS202/20DAS0K:TC2K027407.PNT': None,
       'EWS202/20DAS0K:TC2K027507.PNT': None,
       'EWS202/20DAS0L:PT2L045403.PNT': None,
       'EWS202/20DAS0L:RT2L033106.PNT': None,
       'EWS202/20DAS0L:RT2L033206.PNT': None,
       'EWS202/20DAS0M:RT2M053301.PNT': None,
       'EWS202/20DAS0M:RT2M053401.PNT': None,
       'EWS202/20DAS0M:TC2M053102.PNT': None,
       'EWS202/20DAS0M:TC2M053202.PNT': None,
       'EWS202/20DAS0M:TC2M053205.PNT': None,
       'EWS202/20DISP:STEAMFLOWB.PNT': None,
       'EWS202/20MFLOW:AIN1.PNT': None,
       'EWS202/20MFLOW:AIN10.PNT': None,
       'EWS202/20MFLOW:AIN11.PNT': None,
       'EWS202/20MFLOW:AIN12.PNT': None,
       'EWS202/20MFLOW:AIN2.PNT': None,
       'EWS202/20MFLOW:AIN3.PNT': None,
       'EWS202/20MFLOW:AIN4.PNT': None,
       'EWS202/20MFLOW:AIN5.PNT': None,
       'EWS202/20MFLOW:AIN6.PNT': None,
       'EWS202/20MFLOW:AIN7.PNT': None,
       'EWS202/20MFLOW:AIN8.PNT': None,
       'EWS202/20MFLOW:AIN9.PNT': None
    },
    "SA to PA Ratio": lambda df_before, df_after:np.divide(df_after["Total Secondary Air Flow"].values, df_after["Total Primary Air Flow"].values, out=np.zeros_like(df_after["Total Secondary Air Flow"].values), where=df_after["Total Primary Air Flow"].values!=0),
    "Air to Fuel Ratio": lambda df_before, df_after: np.divide(df_after["Total Secondary Air Flow"].values + df_after["Total Primary Air Flow"].values, df_after["Coal Flow"].values, out=np.zeros_like(df_after["Total Primary Air Flow"].values), where=df_after["Coal Flow"].values!=0),
   # Multiple associations

    "Total Primary Air Flow": {
        "MFLOW2AIN3_PNT": lambda x, scale_back=False: x * 2 if scale_back else x * 0.5,
        "MFLOW2AIN4_PNT": lambda x, scale_back=False: x * 2 if scale_back else x * 0.5,
        "MFLOW2AIN1_PNT": lambda x, scale_back=False: x * 2 if scale_back else x * 0.5,
        "MFLOW2AIN2_PNT": lambda x, scale_back=False: x * 2 if scale_back else x * 0.5,
        "MFLOW2AIN5_PNT": lambda x, scale_back=False: x * 2 if scale_back else x * 0.5,
        "MFLOW2AIN6_PNT": lambda x, scale_back=False: x * 2 if scale_back else x * 0.5,
        "MFLOW2AIN7_PNT": lambda x, scale_back=False: x * 2 if scale_back else x * 0.5,
        "MFLOW2AIN8_PNT": lambda x, scale_back=False: x * 2 if scale_back else x * 0.5,
        "MFLOW2AIN11_PNT": lambda x, scale_back=False: x * 2 if scale_back else x * 0.5,
        "MFLOW2AIN12_PNT": lambda x, scale_back=False: x * 2 if scale_back else x * 0.5,
    },
    "Feed Water Flow": {
        "FT2A010105_PNT": None,
        "FT2A010205_PNT": None,
        "FT2A010305_PNT": None,
    },
    "SH Spray Water Flow": {
        "FT2A011204_PNT": None,
        "FT2A010601_PNT": None
    },
    "Reheat Spray Water Flow": {
        "FT2A010405_PNT": None,
        "FT2A011206_PNT": None
    },
    "Coal Flow": {
        "ZT2B020706_PNT": None,
        "ZT2B021105_PNT": None,
        "ZT2B021206_PNT": None,
        "ZT2B021307_PNT": None,
        "ZT2B021406_PNT": None,
        "ZT2B021606_PNT": None,
    },

    "Main Steam Pressure": {
        "PT2J010501_PNT": None,
        "PT2J010601_PNT": None,
        "PT2J010704_PNT": None,
    },
    "Hot Reheat Steam Pressure": {
        "PT2A010404_PNT": None,
        "PT2A010506_PNT": None,
    },
    "Hot Reheat Steam Temperature": {
        "TC2A012306_PNT": None
    },
    "Mill A Outlet Temperature": {
        "RT2B023401_PNT": None,
        "RT2B023502_PNT": None,
        "RT2B023602_PNT": None,
        "RT2F113301_PNT": None,
        "RT2F113401_PNT": None,
        "RT2F113501_PNT": None
    },
    "Mill B Outlet Temperature": {
        "RT2B023402_PNT": None,
        "RT2B023503_PNT": None,
        "RT2B023603_PNT": None,
        "RT2F124301_PNT": None,
        "RT2F124401_PNT": None,
        "RT2F124501_PNT": None
    },
    "Mill C Outlet Temperature": {
        "RT2B023403_PNT": None,
        "RT2B023504_PNT": None,
        "RT2B023604_PNT": None,
        "RT2G133301_PNT": None,
        "RT2G133401_PNT": None,
        "RT2G133501_PNT": None
    },
    "Mill D Outlet Temperature": {
        "RT2B023404_PNT": None,
        "RT2B023505_PNT": None,
        "RT2B023605_PNT": None,
        "RT2G144301_PNT": None,
        "RT2G144401_PNT": None,
        "RT2G144501_PNT": None,
    },
    "Mill E Outlet Temperature": {
        "RT2B023405_PNT": None,
        "RT2B023506_PNT": None,
        "RT2B023606_PNT": None,
        "RT2H153301_PNT": None,
        "RT2H153401_PNT": None,
        "RT2H153501_PNT": None,
    },
    "Windbox-to-Furnace Pressure": {
        "PT2C030706_PNT": None,
        "PT2C030805_PNT": None,
        "PT2C030407_PNT": None,
        "PT2I172202_PNT": None,
        "PT2C030506_PNT": None,
        "PT2C031101_PNT": None,
        "PT2C030606_PNT": None,
    },
    "Furnace Pressure A": {
        "PT2B020701_PNT":  None,
    },
    "Furnace Pressure B": {
        "PT2B021101_PNT":  None
    },
    "Furnace Pressure C": {
        "PT2B021201_PNT":  None,
        "PT2B021306_PNT":  None,
    },

    "Steam Drum Pressure": None,

    "Bed Pressure": None,

    "Bed Temperature": None,

    "SA Fan A": None,

    "SA Fan B": None,

    # Single Associations

    "ECONOMIZER GAS OUTLET A OXYGEN #1": {
        "AI2B022107_PNT": None
    },
    "ECONOMIZER GAS OUTLET B OXYGEN #1": {
        "AI2B022501_PNT": None
    },

    "Burner Tilt Position 0": {
        "ZT2C030101_PNT": None,
    },
    "Burner Tilt Position 1": {
        "ZT2C030101_PNT": None,
    },
    "Burner Tilt Position 2": {
        "ZT2C030101_PNT": None,
    },
    "Burner Tilt Position 3": {
        "ZT2C030101_PNT": None,
    },
    "Cold Reheat Steam Pressure": {
        "PT2J010805_PNT": None,
    },
    "SH Spray Water Pressure": {
        "PT2B021607_PNT": None,
    },
    "Feed Water Pressure": {
        "PT2J012501_PNT": None,
    },
    "Main Steam Temperature": {
        "TC2M053102_PNT": None
    },
    "Cold Reheat Steam Temperature": {
        "TC2M053205_PNT": None
    },
    "SH Spray Temperature": {
        "RT2A013602_PNT": None
    },
    "Feed Water Temperature": {
        "TC2M053206_PNT": None
    },
    "Main Steam Flow": {
        "DISP2STEAMFLOWB_PNT": None
    },
    "Generator Gross Megawatt": {
        "AI2O013305_PNT": None
    },
    "Bias Burner Tilt Position": {
        "ZT2C030101_PNT": None
    },

    #"FURNACE PRESSURE HIGH": {
    #    "DI2I170101_CIN": None
    #},
    #"FURNACE PRESSURE HIGH ALARM": {
    #    "DI2I172502_CIN": None
    #},
    #"FURNACE PRESSURE LOW": {
    #    "DI2I170102_CIN": None
    #},
    #"FURNACE PRESSURE LOW ALARM": {
    #    "DI2I173401_CIN": None
    #},

    "No.1 FD FAN MOVABLE BLADE  CTL": {
        "AO2B024104_MA": None
    },
    "No.1 FD FAN MOVABLE BLADE  POSI": {
        "ZT2B020506_PNT": None
    },
    "No1 FD FAN MOT CURRENT": {
        "CT2D012607_PNT": None
    },
    "No.2 FD FAN MOVABLE BLADE CTL": {
        "AO2B024404_MA": None
    },
    "No.2 FD FAN MOVABLE BLADE POSI": {
        "ZT2B021605_PNT": None
    },
    "No2 FD FAN MOT CURRENT": {
        "CT2E046302_PNT": None
    },

    "Opening angle of control valve at secondary air fan 1 inlet": None,
    "Opening angle of control valve at secondary air fan 2 inlet": None,
    "O2 CT at Low TEM SHTR OUTL (LE)": None,
    "O2 CT at Low TEM SHTR OUTL (RI)": None
}

TAGS_TO_ACTIONS= {
    "CONST_LIMITS": {
        # Add Excess Oxygen limits.
        "Mill A Outlet Temperature": lambda current_x, delta_x: 45 <= current_x + delta_x <= 70,
        "Mill B Outlet Temperature": lambda current_x, delta_x: 45 <= current_x + delta_x <= 70,
        "Mill C Outlet Temperature": lambda current_x, delta_x: 45 <= current_x + delta_x <= 70,
        "Mill D Outlet Temperature": lambda current_x, delta_x: 45 <= current_x + delta_x <= 70,
        "Mill E Outlet Temperature": lambda current_x, delta_x: 45 <= current_x + delta_x <= 70,
        "Windbox-to-Furnace Pressure": lambda current_x, delta_x: current_x + delta_x <= 2.2,
        "Furnace Pressure A": lambda current_x, delta_x: -100 <= current_x + delta_x <= -5,
        "Furnace Pressure B": lambda current_x, delta_x: -100 <= current_x + delta_x <= -5,
        "Furnace Pressure C": lambda current_x, delta_x: -100 <= current_x + delta_x <= -5,
        "Total Secondary Air Flow": lambda current_x, delta_x: abs(delta_x) <= 0.15 * current_x,
        "Generator Gross Megawatt": lambda current_x, delta_x: current_x + delta_x >= 175 
    },

    "MODELLING_CONSTRAINT_FILTER": {
        # These constraints are based on thresholds for operationability of the boiler and concern training data
        # filtering and used only if they are in MODELLING_CONSTRAINT_VARIABLES Tag Group Mapping
        "Generator Gross Megawatt": lambda x: x >= 250
    },
    "RESCALE_SENSOR":{
        "Feed Water Flow": lambda x, scale_back=False: x * 3.6  if scale_back else x * 0.27777777777,
        "SH Spray Water Flow": lambda x, scale_back=False: x * 3.6  if scale_back else x * 0.27777777777,
        "Coal Flow": lambda x, scale_back=False: x * 3.6  if scale_back else x * 0.27777777777,
        "Main Steam Flow":lambda x, scale_back=False: x * 3.6  if scale_back else x * 0.27777777777,
        "Main Steam Pressure": lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "SH Spray Water Pressure": lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "Feed Water Pressure": lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "Cold Reheat Steam Pressure":lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "Furnace Pressure A": lambda x, scale_back=False: x * 9.80665 if scale_back else x * 0.10197,
        "Furnace Pressure B": lambda x, scale_back=False: x * 9.80665 if scale_back else x * 0.10197,
        "Furnace Pressure C": lambda x, scale_back=False: x * 9.80665 if scale_back else x * 0.10197
    },
    "TARGET_LIMITS": {
        "Efficiency": lambda x: x > 95,
        },

    "GET_EXCESS_OXYGEN_SWEET_SPOT": {
        "ECONOMIZER GAS OUTLET A OXYGEN #1": 3,
        "ECONOMIZER GAS OUTLET B OXYGEN #1": 3
        }
}

TAG_GROUP_MAP={
    "PRESSURE_TAGS": {
        "Main Steam Pressure",
        "Feed Water Pressure",
        "SH Spray Water Pressure",
        "Hot Reheat Steam Pressure",
        "Cold Reheat Steam Pressure"
    },
    "FLOW_TAGS":{
        "Coal Flow",
        "Main Steam Flow",
        "Feed Water Flow",
        "SH Spray Water Flow"
    },
    "RECOMMENDATION_MV_VARIABLES":{
        "Total Secondary Air Flow",
        "Burner Tilt Position 0",
        "Burner Tilt Position 1",
        "Burner Tilt Position 2",
        "Burner Tilt Position 3"
    },
    "RECOMMENDATION_TARGET_VARIABLES":{
        "Efficiency"
    },

    "MODELLING_CONSTRAINT_VARIABLES":{
        "Generator Gross Megawatt"
    },
    "RECOMMENDATION_DIRECTION_VARIABLES":{
        "ECONOMIZER GAS OUTLET A OXYGEN #1",
        "ECONOMIZER GAS OUTLET B OXYGEN #1"
    },
    "DV_VARIABLES":{
        "Coal HHV",
        "Coal Flow",
        "Total Primary Air Flow",
        "Generator Gross Megawatt",
        "No.1 FD FAN MOVABLE BLADE  CTL",
        "No.1 FD FAN MOVABLE BLADE  POSI",
        "No.2 FD FAN MOVABLE BLADE CTL",
        "No.2 FD FAN MOVABLE BLADE POSI",
        "Cold Reheat Steam Pressure",
        "SH Spray Water Pressure",
        "Feed Water Pressure",
        "Main Steam Temperature",
        "Cold Reheat Steam Temperature",
        "Feed Water Flow",
        "SH Spray Water Flow",
        "Reheat Spray Water Flow",
        "Main Steam Pressure",
        "Hot Reheat Steam Pressure"
        },
    "MILL_OUTLET_CONSTRAINTS":{
        "Mill A Outlet Temperature",
        "Mill B Outlet Temperature",
        "Mill C Outlet Temperature",
        "Mill D Outlet Temperature",
        "Mill E Outlet Temperature"
    },
    "MV_DEPENDENT_CONSTRAINTS":{
        "Windbox-to-Furnace Pressure",
        "Furnace Pressure A",
        "Furnace Pressure B",
        "Furnace Pressure C",
        #"FURNACE PRESSURE HIGH",
        #"FURNACE PRESSURE HIGH ALARM",
        #"FURNACE PRESSURE LOW",
        #"FURNACE PRESSURE LOW ALARM"
    },
    "MODELLED_TAGS": {
        "Efficiency"
    },
    "IGNORED_TAGS": {
        "Steam Drum Pressure",
        "Opening angle of control valve at secondary air fan 1 inlet",
        "Opening angle of control valve at secondary air fan 2 inlet",
        "O2 CT at Low TEM SHTR OUTL (LE)",
        "O2 CT at Low TEM SHTR OUTL (RI)",
        "SA Fan A",
        "SA Fan B",
        "Bed Pressure",
        "Bed Temperature",
    }
}