import numpy as np
import sys

if "../../actions" not in sys.path:
    sys.path.append("../../actions")

TAG_ASSOCIATIONS = {

    # Custom Formula associations

    "Total Secondary Air Flow": lambda df_before, df_after: df_before[[
        'V4::DPU1004.SH0065.AALM006502.PV',
        'V4::DPU1004.SH0065.AALM006501.PV',
        'V4::DPU1003.SH0065.AALM006501.PV',
        'V4::DPU1003.SH0065.AALM006502.PV',
        'V4::DPU1003.SH0064.AALM006401.PV',
        'V4::DPU1004.SH0064.AALM006402.PV',
        'V4::DPU1004.SH0064.AALM006401.PV',
        'V4::DPU1003.SH0064.AALM006402.PV'
        ]].values.sum(axis=1),
    "Coal HHV": lambda df_before: np.full(np.array([df_before.shape[0], 1]), 4100),
    "Hot Reheat Steam Flow": None,
    "Efficiency": {
        'V4::DPU1001.HW.AI010205.PV': None,
        'V4::DPU1001.HW.AI020101.PV': None,
        'V4::DPU1001.HW.TC010404.PV': None,
        'V4::DPU1001.HW.TC020304.PV': None,
        'V4::DPU1001.HW.TC020403.PV': None,
        'V4::DPU1001.SH0042.AALM004201.PV': None,
        'V4::DPU1001.SH0043.AALM004301.PV': None,
        'V4::DPU1002.HW.AI020101.PV': None,
        'V4::DPU1002.HW.RT010301.PV': None,
        'V4::DPU1002.SH0041.AALM004101.PV': None,
        'V4::DPU1002.SH0042.AALM004201.PV': None,
        'V4::DPU1002.SH0043.AALM004301.PV': None,
        'V4::DPU1003.HW.AI030302.PV': None,
        'V4::DPU1003.HW.AI030303.PV': None,
        'V4::DPU1003.HW.RT010401.PV': None,
        'V4::DPU1003.HW.RT010402.PV': None,
        'V4::DPU1003.HW.RT010403.PV': None,
        'V4::DPU1003.HW.RT010407.PV': None,
        'V4::DPU1003.HW.TC010605.PV': None,
        'V4::DPU1003.HW.TC010606.PV': None,
        'V4::DPU1003.HW.TC010607.PV': None,
        'V4::DPU1004.HW.AI030302.PV': None,
        'V4::DPU1004.HW.AI030303.PV': None,
        'V4::DPU1004.HW.RT010401.PV': None,
        'V4::DPU1004.HW.RT010402.PV': None,
        'V4::DPU1004.HW.RT010403.PV': None,
        'V4::DPU1004.HW.RT010406.PV': None,
        'V4::DPU1004.HW.TC010605.PV': None,
        'V4::DPU1005.HW.AI020301.PV': None,
        'V4::DPU1005.HW.AI020302.PV': None,
        'V4::DPU1006.HW.AI030101.PV': None,
        'V4::DPU1006.HW.AI030102.PV': None,
        'V4::DPU1007.HW.TC020301.PV': None,
        'V4::DPU1007.HW.TC020302.PV': None
    },
    "SA to PA Ratio": lambda df_before, df_after:np.divide(df_after["Total Secondary Air Flow"].values, df_after["Total Primary Air Flow"].values, out=np.zeros_like(df_after["Total Secondary Air Flow"].values), where=df_after["Total Primary Air Flow"].values!=0),
    "Air to Fuel Ratio": lambda df_before, df_after: np.divide(df_after["Total Secondary Air Flow"].values + df_after["Total Primary Air Flow"].values, df_after["Coal Flow"].values, out=np.zeros_like(df_after["Total Primary Air Flow"].values), where=df_after["Coal Flow"].values!=0),
   # Multiple associations
    "Total Primary Air Flow": {
        "V4::DPU1003.SH0067.AALM006702.PV": lambda x: x,
        "V4::DPU1003.SH0067.AALM006701.PV": lambda x: x
    },
    "Feed Water Flow": {
        "1oo2": None,
        "V4::DPU1001.SH0041.AALM004101.PV": None,
        "V4::DPU1002.SH0041.AALM004101.PV": None,
    },
    "SH Spray Water Flow": {
        "V4::DPU1001.SH0043.AALM004301.PV": None,
        "V4::DPU1002.SH0043.AALM004301.PV": None,
        "V4::DPU1002.SH0042.AALM004201.PV": None,
        "V4::DPU1001.SH0042.AALM004201.PV": None
    },
    "Reheat Spray Water Flow": None,
    "Coal Flow": {
        "V4::DPU1005.HW.AI020301.PV": None,
        "V4::DPU1006.HW.AI030101.PV": None,
        "V4::DPU1005.HW.AI020302.PV": None,
        "V4::DPU1006.HW.AI030102.PV": None
    },
    "Main Steam Pressure": {
        "V4::DPU1002.HW.AI020101.PV": None
    },
    "Hot Reheat Steam Pressure": None,
    "Hot Reheat Steam Temperature": None,
    "Mill A Outlet Temperature": None,
    "Mill B Outlet Temperature": None,
    "Mill C Outlet Temperature": None,
    "Mill D Outlet Temperature": None,
    "Mill E Outlet Temperature": None,
    "Windbox-to-Furnace Pressure": {
        "V4::DPU1003.HW.AI010306.PV": None,
        "V4::DPU1004.HW.AI010306.PV": None
    },
    "Furnace Pressure A": {
        "V4::DPU1003.HW.AI020401.PV": None
    },
    "Furnace Pressure B": {
        "V4::DPU1004.HW.AI020401.PV": None
    },
    "Furnace Pressure C": None,

    "Steam Drum Pressure": {
        "V4::DPU1002.HW.AI010206.PV": None,
        "V4::DPU1002.HW.AI020107.PV": None,
        "V4::DPU1002.HW.AI030104.PV": None,
    },

    "Bed Pressure": {
        "V4::DPU1003.HW.AI010205.PV": None,
        "V4::DPU1004.HW.AI010205.PV": None,
        "V4::DPU1003.HW.AI010206.PV": None,
        "V4::DPU1004.HW.AI010206.PV": None
    },

    # "Bed Temperature": {
    #     "V4::DPU1003.HW.TC020601.PV": None,
    #     "V4::DPU1004.HW.TC020601.PV": None,
    #     "V4::DPU1003.HW.TC020602.PV": None,
    #     "V4::DPU1004.HW.TC020602.PV": None,
    #     "V4::DPU1003.HW.TC020603.PV": None,
    #     "V4::DPU1004.HW.TC020603.PV": None,
    #     "V4::DPU1003.HW.TC020604.PV": None,
    #     "V4::DPU1004.HW.TC020604.PV": None,
    #     "V4::DPU1003.HW.TC020605.PV": None,
    #     "V4::DPU1004.HW.TC020605.PV": None,
    #     "V4::DPU1003.HW.TC020606.PV": None,
    #     "V4::DPU1004.HW.TC020606.PV": None,
    # },

    # "Bed Pressure": None,
    "Bed Temperature": None,

    "SA Fan A": {
        "V4::DPU1003.SH0064.AALM006401.PV": None,
        "V4::DPU1003.SH0065.AALM006501.PV": None,
        "V4::DPU1004.SH0064.AALM006401.PV": None,
        "V4::DPU1004.SH0065.AALM006501.PV": None,
    },

    "SA Fan B": {
        "V4::DPU1003.SH0064.AALM006402.PV": None,
        "V4::DPU1003.SH0065.AALM006502.PV": None,
        "V4::DPU1004.SH0064.AALM006402.PV": None,
        "V4::DPU1004.SH0065.AALM006502.PV": None,
    },

    # Single Associations

    "ECONOMIZER GAS OUTLET A OXYGEN #1": {
        "V4::DPU1003.HW.AI030302.PV": None
    },
    "ECONOMIZER GAS OUTLET B OXYGEN #1": {
        "V4::DPU1004.HW.AI030302.PV": None
    },

    "Burner Tilt Position 0": None,
    "Burner Tilt Position 1": None,
    "Burner Tilt Position 2": None,
    "Burner Tilt Position 3": None,

    "Cold Reheat Steam Pressure": None,

    "SH Spray Water Pressure": {
        "V4::DPU1001.HW.AI010205.PV": None
    },
    "Feed Water Pressure": {
        "V4::DPU1001.HW.AI010205.PV": None
    },
    "Main Steam Temperature": {
        "V4::DPU1001.HW.TC010404.PV": None
    },
    "Cold Reheat Steam Temperature": None,
    "SH Spray Temperature": {
        "V4::DPU1002.HW.RT010301.PV": None
    },
    "Feed Water Temperature": {
        "V4::DPU1002.HW.RT010301.PV": None
    },
    "Main Steam Flow": {
        "V4::DPU1008.SH0055.AALM1.PV": None
    },
    "Generator Gross Megawatt": {
        "V4::DPU1012.HW.AI010203.PV": None
    },
    "Bias Burner Tilt Position": None,

    # "FURNACE PRESSURE HIGH": None,
    # "FURNACE PRESSURE HIGH ALARM": None,
    # "FURNACE PRESSURE LOW": None,
    # "FURNACE PRESSURE LOW ALARM": None,

    "No.1 FD FAN MOVABLE BLADE  CTL": None,
    "No.1 FD FAN MOVABLE BLADE  POSI": None,
    "No1 FD FAN MOT CURRENT": None,
    "No.2 FD FAN MOVABLE BLADE CTL": None,
    "No.2 FD FAN MOVABLE BLADE POSI": None,
    "No2 FD FAN MOT CURRENT": None,

    "Opening angle of control valve at secondary air fan 1 inlet": {
        "V4::DPU1003.HW.AI010203.PV": None
    },
    "Opening angle of control valve at secondary air fan 2 inlet": {
        "V4::DPU1004.HW.AI010203.PV": None
    },
    "O2 CT at Low TEM SHTR OUTL (LE)": {
        "V4::DPU1003.HW.AI030301.PV": None
    },
    "O2 CT at Low TEM SHTR OUTL (RI)": {
        "V4::DPU1004.HW.AI030301.PV": None
    }
}

TAGS_TO_ACTIONS= {
    "CONST_LIMITS": {
        "Windbox-to-Furnace Pressure": lambda current_x, delta_x: current_x + delta_x <= 3.726,
        "O2 CT at Low TEM SHTR OUTL (LE)": lambda current_x, delta_x: (2 <= current_x + delta_x) & (delta_x <= 0.5),
        "O2 CT at Low TEM SHTR OUTL (RI)": lambda current_x, delta_x: (2 <= current_x + delta_x) & (delta_x <= 0.5),
        "Furnace Pressure A": lambda current_x, delta_x: current_x + delta_x <= -5,
        "Furnace Pressure B": lambda current_x, delta_x: current_x + delta_x <= -5,
        "Bed Pressure": lambda current_x, delta_x: current_x + delta_x <= 1000,
        #"Bed Temperature": lambda current_x, delta_x: 790 <= current_x + delta_x <= 900,
        "Total Secondary Air Flow": lambda current_x, delta_x: abs(delta_x) <= 0.15 * current_x,
        "Generator Gross Megawatt": lambda current_x, delta_x: current_x + delta_x >= 50
    },

    "RESCALE_SENSOR":{
        "Windbox-to-Furnace Pressure": lambda x, scale_back=False: x * 1000  if scale_back else x * 0.001,
        "Bed Pressure": lambda x, scale_back=False: x * 0.00981  if scale_back else x * 101.97,
        "Feed Water Flow": lambda x, scale_back=False: x * 3.6  if scale_back else x * 0.27777777777,
        "SH Spray Water Flow": lambda x, scale_back=False: x * 3.6  if scale_back else x * 0.27777777777,
        "Coal Flow": lambda x, scale_back=False: x * 3.6  if scale_back else x * 0.27777777777,
        "Main Steam Flow":lambda x, scale_back=False: x * 3.6  if scale_back else x * 0.27777777777,
        "Main Steam Pressure": lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "Steam Drum Pressure": lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "SH Spray Water Pressure": lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "Feed Water Pressure": lambda x, scale_back=False: x * 0.1  if scale_back else x * 10,
        "Furnace Pressure A": lambda x, scale_back=False: x * 9.80665 if scale_back else x * 0.10197,
        "Furnace Pressure B": lambda x, scale_back=False: x * 9.80665 if scale_back else x * 0.10197,
    },

    "TARGET_LIMITS": {
        "Efficiency": lambda x: x > 90,
        },

    "GET_EXCESS_OXYGEN_SWEET_SPOT": {
        "O2 CT at Low TEM SHTR OUTL (LE)": 3,
        "O2 CT at Low TEM SHTR OUTL (RI)": 3
        }
}

TAG_GROUP_MAP={
    "PRESSURE_TAGS": {
        "Main Steam Pressure",
        "Feed Water Pressure",
        "SH Spray Water Pressure",
        "Steam Drum Pressure",
        "Bed Pressure",
    },
    "FLOW_TAGS":{
        "Coal Flow",
        "Main Steam Flow",
        "Feed Water Flow",
        "SH Spray Water Flow"
    },
    "RECOMMENDATION_MV_VARIABLES":{
        "Total Secondary Air Flow",
    },
    "RECOMMENDATION_TARGET_VARIABLES":{
        "Efficiency",
    },
    "RECOMMENDATION_DIRECTION_VARIABLES":{
        "O2 CT at Low TEM SHTR OUTL (LE)",
        "O2 CT at Low TEM SHTR OUTL (RI)"
    },
    "DV_VARIABLES":{
        "Coal HHV",
        "Coal Flow",
        "Total Primary Air Flow",
        "Generator Gross Megawatt",
        "Opening angle of control valve at secondary air fan 1 inlet",
        "Opening angle of control valve at secondary air fan 2 inlet",
        "ECONOMIZER GAS OUTLET A OXYGEN #1",
        "ECONOMIZER GAS OUTLET B OXYGEN #1",
        },
    "MILL_OUTLET_CONSTRAINTS":{
    },
    "MV_DEPENDENT_CONSTRAINTS":{
        "Windbox-to-Furnace Pressure",
        "Furnace Pressure A",
        "Furnace Pressure B",
        "Bed Pressure",
        # "Bed Temperature"
    },
    'MODELLED_TAGS': {
        "Efficiency"
    },
    "IGNORED_TAGS": {
        "Hot Reheat Steam Flow",
        "Reheat Spray Water Flow",
        "Hot Reheat Steam Pressure",
        "Cold Reheat Steam Pressure",
        "Hot Reheat Steam Temperature",
        "Mill A Outlet Temperature",
        "Mill B Outlet Temperature",
        "Mill C Outlet Temperature",
        "Mill D Outlet Temperature",
        "Mill E Outlet Temperature",
        "Furnace Pressure C",
        "Bias Burner Tilt Position",
        "Burner Tilt Position 0",
        "Burner Tilt Position 1",
        "Burner Tilt Position 2",
        "Burner Tilt Position 3",
        "Cold Reheat Steam Temperature",
        "No.1 FD FAN MOVABLE BLADE  CTL",
        "No.1 FD FAN MOVABLE BLADE  POSI",
        "No1 FD FAN MOT CURRENT",
        "No.2 FD FAN MOVABLE BLADE CTL",
        "No.2 FD FAN MOVABLE BLADE POSI",
        "No2 FD FAN MOT CURRENT",
        # "FURNACE PRESSURE HIGH",
        # "FURNACE PRESSURE HIGH ALARM",
        # "FURNACE PRESSURE LOW",
        # "FURNACE PRESSURE LOW ALARM"
        # "Bed Pressure",
        "Bed Temperature"
        }
}