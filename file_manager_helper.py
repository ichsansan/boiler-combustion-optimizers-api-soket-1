import os
import sys
import hashlib
from tensorflow.keras.models import model_from_json
from typing import Dict, List, Any, Tuple
from sklearn.pipeline import Pipeline
import pandas as pd

if "contracts" not in sys.path:
    sys.path.append("contracts")

from contracts.tag_contract_factory import TagContract
#import dill as pickle
import pickle5 as pickle

ZIPPED_FILES_EXTENSION = ".bin"
ZIPPED_JSON_FILES_EXTENSION = ".json"
ZIPPED_H5_FILES_EXTENSION = ".h5"
DATASET_EXTENSION = ".csv"


def load_forecasting_model(directory: str, model_name: str):
    pipeline_path = os.path.join(directory, f"{model_name}_pipeline{ZIPPED_FILES_EXTENSION}")
    json_model_path = os.path.join(directory, f"{model_name}_model{ZIPPED_JSON_FILES_EXTENSION}")
    h5_model_path = os.path.join(directory, f"{model_name}_model{ZIPPED_H5_FILES_EXTENSION}")

    json_file = open(json_model_path, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    loaded_model.load_weights(h5_model_path)

    pipeline = pickle.load(open(pipeline_path, 'rb'))
    pipeline["model"].model = loaded_model

    return pipeline


def save_forecasting_model(pipeline: Pipeline, directory: str, model_name: str):
    os.makedirs(directory)

    pipeline_path = os.path.join(directory, f"{model_name}_pipeline{ZIPPED_FILES_EXTENSION}")
    json_model_path = os.path.join(directory, f"{model_name}_model{ZIPPED_JSON_FILES_EXTENSION}")
    h5_model_path = os.path.join(directory, f"{model_name}_model{ZIPPED_H5_FILES_EXTENSION}")

    model = pipeline["model"].model

    model_json = model.to_json()
    with open(json_model_path, "w") as json_file:
        json_file.write(model_json)

    model.save_weights(h5_model_path)

    pipeline["model"].model = None
    pickle.dump(pipeline, open(pipeline_path, 'wb'))
    pipeline["model"].model = model


def save_generative_model_xgb(model_name: str, directory: str, pipeline: Pipeline):
    if not os.path.exists(directory):
        os.makedirs(directory)

    model_path = os.path.join(directory, model_name)
    os.makedirs(model_path)

    pipeline_path = os.path.join(model_path, f"{model_name}_pipeline{ZIPPED_FILES_EXTENSION}")
    pickle.dump(pipeline, open(pipeline_path, 'wb'))


def load_generative_model_xgb(model_name: str, directory: str) -> Pipeline:
    model_path = os.path.join(directory, model_name)
    pipeline_path = os.path.join(model_path, f"{model_name}_pipeline{ZIPPED_FILES_EXTENSION}")
    pipeline = pickle.load(open(pipeline_path, 'rb'))

    return pipeline


def save_model_converter(model_name: str, directory: str, converter: Any, input_tags: List[str]):
    if not os.path.exists(directory):
        os.makedirs(directory)

    model_path = os.path.join(directory, model_name)
    os.makedirs(model_path)

    converter_path = os.path.join(model_path, f"{model_name}_model_converter{ZIPPED_FILES_EXTENSION}")
    input_tags_path = os.path.join(model_path, f"{model_name}_input_tags{ZIPPED_FILES_EXTENSION}")
    pickle.dump(converter, open(converter_path, 'wb'))
    pickle.dump(input_tags, open(input_tags_path, 'wb'))


def load_model_converter(model_name: str, directory: str) -> Tuple[Any, List[str]]:
    model_path = os.path.join(directory, model_name)

    converter_path = os.path.join(model_path, f"{model_name}_model_converter{ZIPPED_FILES_EXTENSION}")
    input_tags_path = os.path.join(model_path, f"{model_name}_input_tags{ZIPPED_FILES_EXTENSION}")

    converter = pickle.load(open(converter_path, 'rb'))
    input_tags = pickle.load(open(input_tags_path, 'rb'))
    return (converter, input_tags)


def save_tag_contract(directory: str, contract: TagContract):
    h = hashlib.sha256(contract.alias.encode('utf8'))
    name = h.hexdigest()

    if not os.path.exists(directory):
        os.makedirs(directory)

    contract_path = os.path.join(directory, f"{name}_contract{ZIPPED_FILES_EXTENSION}")
    pickle.dump(contract, open(contract_path, 'wb'))


def load_tag_contract(contract_alias: str, directory: str) -> TagContract:
    h = hashlib.sha256(contract_alias.encode('utf8'))
    name = h.hexdigest()

    contract_path = os.path.join(directory, f"{name}_contract{ZIPPED_FILES_EXTENSION}")
    contract = pickle.load(open(contract_path, 'rb'))

    return contract


def get_dataset_file_name(dataset_tag: str, dataset_suffix: str) -> str:
    return f"dataset_{dataset_tag}_{dataset_suffix}{DATASET_EXTENSION}"


def save_pandas_dataset(dataset: pd.DataFrame, dataset_tag: str, dataset_suffix: str, directory: str):
    dataset.to_csv(
        os.path.join(directory, get_dataset_file_name(dataset_tag=dataset_tag, dataset_suffix=dataset_suffix)),
        index=False)
