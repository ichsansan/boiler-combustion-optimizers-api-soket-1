import config
from prediction import MainFlow, Prediction
import time

debug_mode = True
fetch_realtime = True
unit = 'TAA2'
fetch_type = 'realtime'

recommendation = None
# while recommendation is None:
start_time = time.time()

# determine fetching type (from realtime data or provided excel)
if fetch_type == 'realtime':
    fetch_realtime = True
elif fetch_type == 'excel':
    fetch_realtime = False

mainflow = MainFlow(unit, debug_mode=debug_mode, fetch_realtime=fetch_realtime)
mainflow.load_saved_data()
mainflow.read_realtime_data(n_minutes=config.HORIZON_STEP)
mainflow.pre_calculate_tags()
message, curr_efficiency = mainflow.predict_inverse_mapping()

mainflow.realtime_df.to_csv('data/debugging_outputs/Realtime DF.csv')
mainflow.original_realtime_df.to_csv('data/debugging_outputs/Original Realtime DF.csv')

mainflow.post_calculate_tags()
# mainflow.save_to_db()
if mainflow.mv_recomendations_df is not None:
    if len(mainflow.mv_recomendations_df) > 0:
        recommendation = mainflow.mv_recomendations_df
time_logs = mainflow.get_time_logs()

end_time = time.time()

saved_data_time = time_logs['Load Saved Data']
fetch_sensor_time = time_logs['Fetch Sensor Data']
prediction_time = time_logs['Prediction Time']
# saving_db_time = time_logs['Saving to DB Time']

# printing logs on server for debugging purpose
print(flush=True)
print('###################################', flush=True)
print(f'Version: {config.DEPLOYMENT_CURR_VERSION}', flush=True)
print(f'Latest Update: {config.DEPLOYMENT_LAST_UPDATE}', flush=True)
print('API: BAT Combustion', flush=True)
print('Status: Success', flush=True)
print(f'Message: {message}', flush=True)
print(f'Unit: {unit}', flush=True)
print(f'Total Time: {(end_time - start_time) / 60} minutes', flush=True)
print(f"Load Saved Data Time: {saved_data_time}", flush=True)
print(f"Fetch Sensor Data Time: {fetch_sensor_time}", flush=True)
print(f"Prediction Time: {prediction_time}", flush=True)
# print(f"Saving to DB Time: {saving_db_time}", flush=True)
print(f"MV recommendation df: {recommendation}")
print('###################################', flush=True)
# break
# time.sleep(30)
