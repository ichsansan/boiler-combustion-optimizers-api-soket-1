DB_MAPPING_GCP = {
    'TAA1': {
        'username': 'smlds',
        'password': 'SMLds2021!',
        'host': '35.219.48.62',
        'db_name': 'db_bat_tja1',
    },
    'TAA2': {
        'username': 'smlds',
        'password': 'SMLds2021!',
        'host': '35.219.48.62',
        'db_name': 'db_bat_tja2',
    },
    'RBG1': {
        'username': 'smlds',
        'password': 'SMLds2021!',
        'host': '35.219.48.62',
        'db_name': 'db_bat_rbg1',
    },
    'RBG2': {
        'username': 'smlds',
        'password': 'SMLds2021!',
        'host': '35.219.48.62',
        'db_name': 'db_bat_rbg2',
    },
    'PCT1': {
        'username': 'smlds',
        'password': 'SMLds2021!',
        'host': '35.219.48.62',
        'db_name': 'db_bat_pct1',
    },
    'PCT2': {
        'username': 'smlds',
        'password': 'SMLds2021!',
        'host': '35.219.48.62',
        'db_name': 'db_bat_pct2',
    },
}

DB_MAPPING = {
    'TAA1': {
        'username': 'root',
        'password': 'P@ssw0rd',
        'host': '192.168.1.10',
        'db_name': 'db_bat_tja1',
        'tb_data_source': 'tb_combustion_model_input'
    },
    'TAA2': {
        'username': 'root',
        'password': 'P@ssw0rd',
        'host': 'localhost:3306',  # '192.168.1.11',
        'db_name': 'db_bat_tja2',
        'tb_data_source': 'tb_combustion_model_input'
    },
    'RBG1': {
        'username': 'root',
        'password': 'P@ssw0rd',
        'host': '10.7.33.241:3308',
        'db_name': 'db_bat_rmb2',
    },
    'RBG2': {
        'username': 'root',
        'password': 'P@ssw0rd',
        'host': '10.7.33.241:3307',
        'db_name': 'db_bat_rmb1',
    },
    'PCT1': {
        'username': 'root',
        'password': 'P@ssw0rd',
        'host': 'localhost:33006',
        'db_name': 'db_bat_pct1'
    },
    'PCT2': {
        'username': 'root',
        'password': 'P@ssw0rd',
        'host': 'localhost:33006',
        'db_name': 'db_bat_pct2'
    },
}
