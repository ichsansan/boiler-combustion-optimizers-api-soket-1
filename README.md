# Boiler Combustion Optimizer API

This is a recommendation system based on Reinforcement Learning. It recommends a set of Modifiable Variables(MV) values which lead to optimal combustion process. The optimal combustion process criteria are combustion efficiency and excess oxygen. The Reinforcement Learning system consists of two types of models. The first kind generates MV for better exploration of the environment. The second kind of models give a feedback about the environment's conditions. According to these environment conditions we assess the reward for choosing the best set of MVs.

The system is meant to be improved with the time by calibrating the models with the latest data. That is why we introduce various iterations of the models which are reflecting their current state.

For keeping track of the model performance along their calibration for each update or introduction of a model we add new a itteration folder in `iteration_wise_models_results`. In each iteration and each model and each powerplant unit, we have changes for, we add the following resources:

* the result tables with the metrics. We distinguish between 3 kinds of result tables: total, variables level, and instances level and for each level we have different details.

* the result plots. The result plots give visual indication about the performance of the models and for different kinds of models we have different kinds of visualizations. For the generative models we have collection of scatter plots and for the environment models we have line charts.